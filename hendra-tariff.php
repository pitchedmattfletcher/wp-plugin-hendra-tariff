<?php

/**
* Plugin Name: Hendra Tariff
* Plugin URI: https://www.pitched.co.uk/
* Description: Manage the Hendra Tariff
* Version: 1.2
* Author: Pitched
* Author URI: https://www.pitched.co.uk/
**/

// Imports
require('backend/php/other/hendra-tariff-db.php');
require('backend/php/daos/dates-config-dao.php');
require('backend/php/daos/ranges-dao.php');
require('backend/php/daos/statics-dao.php');
require('backend/php/daos/models-dao.php');
require('backend/php/daos/person-types-dao.php');
require('backend/php/daos/pitch-types-dao.php');
require('backend/php/daos/extras-dao.php');
require('backend/php/daos/holiday-homes-tariff-dao.php');
require('backend/php/daos/touring-tariff-dao.php');
require('backend/php/daos/glamping-pods-dao.php');
require('backend/php/daos/glamping-tariff-dao.php');

require('backend/php/pages/dates-config-page.php');
require('backend/php/pages/holiday-homes-ranges-page.php');
require('backend/php/pages/holiday-homes-caravans-page.php');
require('backend/php/pages/holiday-homes-import-page.php');
require('backend/php/pages/touring-person-types-page.php');
require('backend/php/pages/touring-pitch-types-page.php');
require('backend/php/pages/touring-extras-page.php');
require('backend/php/pages/touring-import-page.php');
require('backend/php/pages/glamping-pods-page.php');
require('backend/php/pages/glamping-import-page.php');


// On plugin activation, create the database.
register_activation_hook(__FILE__, function () {
    $db = new HendraTariffDB();
    $db->create();
});

// Add the top level menu item
add_action('admin_menu', function(){
    add_menu_page('Hendra Tariff', 'Hendra Tariff', 'manage_options', 'hendra-tariff', false, 'data:image/svg+xml;base64,' . base64_encode('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20" height="20"><path fill="#a0a5a9" d="M12.1 13.5l-6.8 7-5.2-5.4L11.7 3.8l.3-.3.3.3 11.6 11.3-5.1 5.3-6.7-6.9z"/></svg>'), 100);
});

// Remove the top level item for the 'Hendra Tariff' tab
add_action('admin_head', function () {
    ?> <style>
        #toplevel_page_hendra-tariff a[href="admin.php?page=hendra-tariff"] { pointer-events: none!important; }
        #toplevel_page_hendra-tariff .wp-first-item { display: none!important; }
    </style> <?php
});


// Register AJAX functions with wordpress.
$datesConfigDAO = new DatesConfigDAO();
$datesConfigDAO->registerAjax();
$staticsDAO = new StaticsDAO();
$staticsDAO->registerAjax();
$rangesDAO = new RangesDAO();
$rangesDAO->registerAjax();
$modelsDAO = new ModelsDAO();
$modelsDAO->registerAjax();
$holidayHomesTariffDAO = new HolidayHomesTariffDAO();
$holidayHomesTariffDAO->registerAjax();
$personTypesDAO = new PersonTypesDAO();
$personTypesDAO->registerAjax();
$pitchTypesDAO = new PitchTypesDAO();
$pitchTypesDAO->registerAjax();
$extrasDAO = new ExtrasDAO();
$extrasDAO->registerAjax();
$touringTariffDAO = new TouringTariffDAO();
$touringTariffDAO->registerAjax();
$glampingPodsDAO = new GlampingPodsDAO();
$glampingPodsDAO->registerAjax();
$glampingTariffDAO = new GlampingTariffDAO();
$glampingTariffDAO->registerAjax();

// Create the plugin backend pages
new DatesConfigPage();
new HolidayHomesRangesPage();
new HolidayHomesCaravansPage();
new HolidayHomesImportPage();
new TouringPersonTypesPage();
new TouringPitchTypesPage();
new TouringExtrasPage();
new TouringImportPage();
new GlampingPodsPage();
new GlampingImportPage();


// Init the shortcode functionality
add_shortcode('hendra_tariff', function ($attr) {
    wp_enqueue_style('hendra-tariff-frontend', plugin_dir_url( __FILE__ ) . 'frontend/css/hendra-tariff.min.css', false, false);
    wp_enqueue_script('hendra-tariff-frontend', plugin_dir_url( __FILE__ ) . 'frontend/js/hendra-tariff.js', array('jquery'), false, false);
    ob_start();
    $options = shortcode_atts(array(), $attrs); ?>
    <div class="hendra-tariff-shortcoded"></div>
    <?php 
    $return_string = ob_get_contents(); 
    ob_end_clean(); 
    return $return_string;
});


?>