<?php 

class TouringTariffDAO {

    public function import ($rows) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Now get extras, pitch types and person types from the db
        $codes = $wpdb->get_results("
            SELECT * FROM (
                SELECT code AS code, id, 'extra' as type FROM {$prefix}hendra_tariff_touring_extra
                UNION ALL
                SELECT code, id, 'person_type' FROM {$prefix}hendra_tariff_touring_person_type
                UNION ALL
                SELECT code, id, 'pitch_type' FROM {$prefix}hendra_tariff_touring_pitch_type
            ) tb
        ");
        
        // Map codes to new format.
        $codes = array_map(function ($code) { 
            return array(
                'id' => $code->id,
                'code' => $code->code,
                'type' => $code->type
            ); 
        }, $codes);

        // Set up an array of invalid model names
        $invalid_codes = array();

        // Set up an array of value insert statements
        $insert_values = array();

        // Now loop through rows...
        foreach ($rows as $row) {

            // Reference the model name
            $code = $row['code'];

            // If the code doesn't exist in the DB
            if (!in_array($code, array_map(function ($code) { return $code['code']; }, $codes))) {

                // And if we don't already know that this code doesn't exist,
                // Add it to the invalid codes array so we can return it later.
                if (!in_array($code, $invalid_codes)) {
                    array_push($invalid_codes, $code);
                }

            // Else the code does exist in the DB
            } else {

                // Convert date from dd/mm/yyyy to yyyy-mm-dd 
                $arrival_date = DateTime::createFromFormat('d/m/Y', $row['arrival_date']);
                if ($arrival_date === false) {
                    return array (
                        'success' => false,
                        'message' => 'Error importing file. Invalid date format: ' . $row['arrival_date'],
                    );
				}
                $arrival_date = $arrival_date->format('Y-m-d');
                $arrival_date = $arrival_date . ' 00:00:00';

                // Get code id and type
                $code_id = false;
                $code_type = false;
                for ($i = 0; $i < count($codes); $i++) {
                    if ($codes[$i]['code'] == $code) {
                        $code_id = $codes[$i]['id'];
                        $code_type = $codes[$i]['type'];
                        break;
                    }
                }

                // If code type was found
                if ($code_id != false && $code_type == 'extra') {
                    array_push($insert_values, '(NULL, NULL, ' . (int) $code_id . ', "'. $arrival_date . '", 1, ' . $row['price'] . ', ' . $row['discount'] . ')');
                } else if ($code_id != false && $code_type == 'person_type') {
                    array_push($insert_values, '(' . (int) $code_id . ', NULL, NULL, "'. $arrival_date . '", 1, ' . $row['price'] . ', ' . $row['discount'] . ')');
                } else if ($code_id != false && $code_type == 'pitch_type') {
                    array_push($insert_values, '(NULL, ' . (int) $code_id . ', NULL, "'. $arrival_date . '", 1, ' . $row['price'] . ', ' . $row['discount'] . ')');
                }
 
            }
        }

        // Now, if the invalid model names array contains any elements,
        // there was an error so return success as false etc.
        if (count($invalid_codes) > 0) {
            return array (
                'success' => false,
                'message' => 'Error importing file. Codes ' . implode(", ", $invalid_codes) . ' were not found in the database.',
            );
        }

        // Now everythings all good, delete the current rows from calendar
        $wpdb->query("DELETE FROM {$prefix}hendra_tariff_touring_calendar");

        // Create the insert query for the calendar, using the query we were building earlier
        $insert_query = "INSERT INTO {$prefix}hendra_tariff_touring_calendar (person_type_id, pitch_type_id, extra_id, arrival_date, duration, price, discount) VALUES ";
        $insert_query .= implode( ",\n", $insert_values);

        // Run the insert query. It returns the amount of rows inserted
        $insert_query_result = $wpdb->query($insert_query);

        // Return result
        return array(
            'success' => true,
            'message' => 'Successfully imported ' . $insert_query_result . '/' . count($rows) . ' rows.',
        );

    }
    
    public function export () {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $rows = $wpdb->get_results("
            SELECT
                DATE_FORMAT(CAST(touring_cal.arrival_date as DATE), '%d/%m/%Y') as arrival_date,
                CASE
                    WHEN extras.code IS NULL AND person_types.code IS NULL THEN pitch_types.code
                    WHEN extras.code IS NULL AND pitch_types.code IS NULL THEN person_types.code
                    WHEN pitch_types.code IS NULL AND person_types.code IS NULL THEN extras.code
                    ELSE NULL
                END as code,
                touring_cal.price as price,
                touring_cal.discount as discount
            FROM 
                {$prefix}hendra_tariff_touring_calendar as touring_cal
            LEFT JOIN
                {$prefix}hendra_tariff_touring_extra as extras
            ON 
                touring_cal.extra_id = extras.id
            LEFT JOIN
                {$prefix}hendra_tariff_touring_person_type as person_types
            ON 
                touring_cal.person_type_id = person_types.id
            LEFT JOIN
                {$prefix}hendra_tariff_touring_pitch_type as pitch_types
            ON 
                touring_cal.pitch_type_id = pitch_types.id
            ORDER BY
                touring_cal.arrival_date ASC, touring_cal.extra_id, touring_cal.pitch_type_id, touring_cal.person_type_id
        ");
        return array(
            'success' => true,
            'message' => 'Exported ' . count($rows) .  ' rows.',
            'data' => $rows,
        );
    }

    public function clear () {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $result = $wpdb->query("DELETE FROM {$prefix}hendra_tariff_touring_calendar");
        return array(
            'success' => true,
            'message' => 'Deleted ' . $result . ' rows.',
        );
    }

    public function getByYearMonth($year, $month) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $rows = $wpdb->get_results("
            SELECT
                DATE_FORMAT(CAST(touring_cal.arrival_date as DATE), '%d/%m/%Y') as arrival_date,
                CASE
                    WHEN extras.code IS NULL AND person_types.code IS NULL THEN pitch_types.code
                    WHEN extras.code IS NULL AND pitch_types.code IS NULL THEN person_types.code
                    WHEN pitch_types.code IS NULL AND person_types.code IS NULL THEN extras.code
                    ELSE NULL
                END as code,
                CASE
                    WHEN extras.code IS NULL AND person_types.code IS NULL THEN pitch_types.name
                    WHEN extras.code IS NULL AND pitch_types.code IS NULL THEN person_types.name
                    WHEN pitch_types.code IS NULL AND person_types.code IS NULL THEN extras.name
                    ELSE NULL
                END as name,
                CASE
                    WHEN extras.code IS NULL AND person_types.code IS NULL THEN 'pitch_type'
                    WHEN extras.code IS NULL AND pitch_types.code IS NULL THEN 'person_type'
                    WHEN pitch_types.code IS NULL AND person_types.code IS NULL THEN  'extra'
                    ELSE NULL
                END as type,
                touring_cal.price as price,
                touring_cal.discount as discount
            FROM 
                {$prefix}hendra_tariff_touring_calendar as touring_cal
            LEFT JOIN
                {$prefix}hendra_tariff_touring_extra as extras
            ON 
                touring_cal.extra_id = extras.id
            LEFT JOIN
                {$prefix}hendra_tariff_touring_person_type as person_types
            ON 
                touring_cal.person_type_id = person_types.id
            LEFT JOIN
                {$prefix}hendra_tariff_touring_pitch_type as pitch_types
            ON 
                touring_cal.pitch_type_id = pitch_types.id
            WHERE 
                YEAR(touring_cal.arrival_date) >= $year
            AND 
                MONTH(touring_cal.arrival_date) = $month
            ORDER BY
                touring_cal.arrival_date ASC, touring_cal.extra_id, touring_cal.pitch_type_id, touring_cal.person_type_id        
        ");
        return array(
            'success' => true,
            'message' => 'Retrieved Touring Tariff successfully.',
            'data' => $rows,
        );
    }

    public function getCount () {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $result = $wpdb->get_results("SELECT COUNT(*) as amount FROM {$prefix}hendra_tariff_touring_calendar");
        return $result[0]->amount;
    }

    public function getExample () {
        global $wpdb;
        $prefix = $wpdb->prefix;


        // Create the array to return
        $return = array();

        // Append the registered person types
        $person_types = $wpdb->get_results("SELECT code as code FROM {$prefix}hendra_tariff_touring_person_type");
        foreach ($person_types as $person_type) {
            array_push($return, array(
                'arrival_date' => '02/05/2020',
                'code' => $person_type->code,
                'price' => mt_rand (1*10, 10*10) / 10,
                'discount' => 0,
            ));
        }

        // Append the registered pitch types
        $pitch_types = $wpdb->get_results("SELECT code as code FROM {$prefix}hendra_tariff_touring_pitch_type");
        foreach ($pitch_types as $pitch_type) {
            array_push($return, array(
                'arrival_date' => '02/05/2020',
                'code' => $pitch_type->code,
                'price' => mt_rand (1*10, 10*10) / 10,
                'discount' => 0,
            ));
        }

        // Append the registered person types
        $extras = $wpdb->get_results("SELECT code as code FROM {$prefix}hendra_tariff_touring_extra");
        foreach ($extras as $extra) {
            array_push($return, array(
                'arrival_date' => '02/05/2020',
                'code' => $extra->code,
                'price' => mt_rand (1*10, 10*10) / 10,
                'discount' => 0,
            ));
        }

        return $return;

    }

    public function registerAjax () {
        add_action("wp_ajax_hendra_tariff_touring_import", function () {
            $rows = $_POST['args']['rows'];
            $response = $this->import($rows);
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_touring_export", function () {
            $response = $this->export();
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_touring_clear", function () {
            $response = $this->clear();
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_touring_get_by_year_month", function () {
            $year = (int) $_POST['args']['year'];
            $month = (int) $_POST['args']['month'];
            $response = $this->getByYearMonth($year, $month);
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_nopriv_hendra_tariff_touring_get_by_year_month", function () {
            $year = (int) $_POST['args']['year'];
            $month = (int) $_POST['args']['month'];
            $response = $this->getByYearMonth($year, $month);
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_touring_get_example", function () {
            $response = $this->getExample();
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_touring_get_count", function () {
            $response = $this->getCount();
            echo json_encode($response);
            exit;
        });
    }


}
