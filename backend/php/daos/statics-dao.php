<?php 

/**
 * Statics DAO.
 */
class StaticsDAO {

    /**
     * Get all statics
     */
    public function get () {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $response = array(
            'success' => true,
            'message' => 'Retrieved Statics successfully.',
            'data' => array()
        );
        $result = $wpdb->get_results("
            SELECT 
                {$prefix}hendra_tariff_static.id,
                {$prefix}hendra_tariff_static.name,
                {$prefix}hendra_tariff_static.order,
                {$prefix}hendra_tariff_static.flash,
                {$prefix}hendra_tariff_static_range.id as range_id,
                {$prefix}hendra_tariff_static_range.name as range_name,
                COUNT({$prefix}hendra_tariff_static_model.id) as amount_models
            FROM 
                {$prefix}hendra_tariff_static
            LEFT JOIN 
                {$prefix}hendra_tariff_static_range
            ON 
                {$prefix}hendra_tariff_static.range_id = {$prefix}hendra_tariff_static_range.id
            LEFT JOIN 
                {$prefix}hendra_tariff_static_model
            ON 
                {$prefix}hendra_tariff_static.id = {$prefix}hendra_tariff_static_model.static_id
            GROUP BY 
                {$prefix}hendra_tariff_static.id
            ORDER BY 
                {$prefix}hendra_tariff_static_range.name ASC, 
                {$prefix}hendra_tariff_static.order ASC;
        ");
        $response['data'] = $result;
        return $response;
    }

    public function getStaticByRangeID ($range_id) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $response = array(
            'success' => true,
            'message' => 'Retrieved Statics successfully.',
            'data' => array()
        );
        $result = $wpdb->get_results("
            SELECT 
                {$prefix}hendra_tariff_static.id,
                {$prefix}hendra_tariff_static.name,
                {$prefix}hendra_tariff_static.flash
            FROM 
                {$prefix}hendra_tariff_static
            LEFT JOIN 
                {$prefix}hendra_tariff_static_range
            ON 
                {$prefix}hendra_tariff_static.range_id = {$prefix}hendra_tariff_static_range.id
            WHERE 
                {$prefix}hendra_tariff_static_range.id = $range_id
            GROUP BY 
                {$prefix}hendra_tariff_static.id
            ORDER BY 
                {$prefix}hendra_tariff_static_range.order ASC, 
                {$prefix}hendra_tariff_static.order ASC;
        ");
        $response['data'] = $result;
        return $response;
    }

    /**
     * Create a static
     */
    public function create ($static) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference input
        $model = $_POST['args']['static'];
        $static_name = trim($static['name']);
        $static_range_id = (int) $static['range_id'];
        $static_flash = $static['flash'] ? trim($static['flash']) : null;
        $static_order = (int) $static['order'] ?? 0;

        // If model name is empty
        if (!isset($static_name) || trim($static_name) === '') {
            echo json_encode(array(
                'success' => false,
                'message' => 'A static requires a name.',
            ));
            exit;
        }

        // If model range id is empty
        if (!isset($static_range_id)) {
            return array(
                'success' => false,
                'message' => 'A static must be assigned a range.',
            );
            exit;
        }

        // Check if a static with this name already exists
        $result = $wpdb->get_results("SELECT name FROM {$prefix}hendra_tariff_static WHERE name = '$static_name' LIMIT 1");
        if (count($result) > 0) {
            return array(
                'success' => false,
                'message' => 'A static with this name already exists.',
            );
            exit;
        }

        // Check if a range with this range_id actually exists
        $result = $wpdb->get_results("SELECT id FROM {$prefix}hendra_tariff_static_range WHERE id = $static_range_id LIMIT 1");
        if (count($result) == 0) {
            return array(
                'success' => false,
                'message' => 'The assigned range does not exist.',
            );
            exit;
        }

        // Run the query. It returns the amount of rows inserted.
        $amount = $wpdb->insert("{$prefix}hendra_tariff_static", array(
            'name' => $static_name,
            'range_id' => $static_range_id,
            'flash' => $static_flash,
            'order' => $static_order
        ));

        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Static added successfully.',
                'model' => array(
                    'id' => $wpdb->insert_id,
                    'name' => $static_name,
                    'range_id' => $static_range_id,
                ),
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Unable to add Static.',
                'model' => null,
            );
        }

        // Return response
        return $response;
    }

    /**
     * Update a static
     */
    public function update ($static) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference input
        $static_id = (int) $static['id'];
        $static_name = trim($static['name']);
        $static_range_id = (int) $static['range_id'];
        $static_flash = $static['flash'] ? trim($static['flash']) : null;
        $static_order = (int) $static['order'] ?? 0;

        // If model name is empty
        if (!isset($static_name) || trim($static_name) === '') {
            return array(
                'success' => false,
                'message' => 'A static requires a name.',
            );
            exit;
        }
    
        // If model range id is empty
        if (!isset($static_range_id)) {
            return array(
                'success' => false,
                'message' => 'A static must be assigned a range.',
            );
            exit;
        }
    
        // Check if a static with this name already exists
        $result = $wpdb->get_results("SELECT name FROM {$prefix}hendra_tariff_static WHERE name = '$static_name' AND id != $static_id LIMIT 1");
        if (count($result) > 0) {
            return array(
                'success' => false,
                'message' => 'A static with this name already exists.',
            );
            exit;
        }
    
        // Check if a range with this range_id actually exists
        $result = $wpdb->get_results("SELECT id FROM {$prefix}hendra_tariff_static_range WHERE id = $static_range_id LIMIT 1");
        if (count($result) == 0) {
            return array(
                'success' => false,
                'message' => 'The assigned range does not exist.',
            );
            exit;
        }
    
        // Run the query. It returns the amount of rows updated.
        $amount = $wpdb->update("{$prefix}hendra_tariff_static", array(
            'name' => $static_name,
            'range_id' => $static_range_id,
            'flash' => $static_flash,
            'order' => $static_order
        ), array (
            'id' => $static_id,
        ));
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Static updated successfully.',
                'model' => array(
                    'id' => $static_id,
                    'name' => $static_name,
                    'range_id' => $static_range_id,
                    'order' => $static_order
                ),
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Static with id ' . $static_id . ' was not found.',
                'model' => null,
            );
        }

        // Return response
        return $response;

    }

    /**
     * Delete a static
     */
    public function delete ($static_id) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference input
        $static_id = (int) $static_id;
    
        // Run the query. It returns the amount of rows deleted.
        $amount = $wpdb->delete("{$prefix}hendra_tariff_static", array(
            'id' => $static_id
        ));
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Static deleted successfully.',
                'id' => $static_id,
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Unable to delete Static.',
                'model' => null,
            );
        }
    
        return $response;
    }

    /**
     * Register ajax with wordpress.
     */
    public function registerAjax () {
        add_action("wp_ajax_hendra_tariff_get_statics", function () {
            $response = $this->get();
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_create_static', function () {
            $static = $_POST['args']['static'];
            $response = $this->create($static);
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_update_static', function () {
            $static = $_POST['args']['static'];
            $response = $this->update($static);
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_delete_static', function () {
            $static_id = $_POST['args']['static_id'];
            $response = $this->delete($static_id);
            echo json_encode($response);
            exit;
        });
    }

}