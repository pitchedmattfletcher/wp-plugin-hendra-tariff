<?php 

class PitchTypesDAO {

    public function get () {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $response = array(
            'success' => true,
            'message' => 'Retrieved Pitch Types successfully.',
            'data' => array()
        );
        $result = $wpdb->get_results("
            SELECT 
                {$prefix}hendra_tariff_touring_pitch_type.id, 
                {$prefix}hendra_tariff_touring_pitch_type.name,
                {$prefix}hendra_tariff_touring_pitch_type.code
            FROM 
                {$prefix}hendra_tariff_touring_pitch_type
            ORDER BY 
                {$prefix}hendra_tariff_touring_pitch_type.id
            ASC
        ");
        $response['data'] = $result;
        return $response;
    }


    public function create ($pitch_type) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference range name
        $pitch_type_name = trim($pitch_type['name']);
        $pitch_type_code = trim($pitch_type['code']);
    
        // If model name is empty
        if ($pitch_type_name == '' || $pitch_type_code == '') {
            return array(
                'success' => false,
                'message' => 'A pitch type requires a name and a code.',
            );
        }

        // Check if an extra, person type or pitch type with this code already exists.
        $result = $wpdb->get_results("
            SELECT 
                1 
            FROM (
                SELECT code AS code FROM {$prefix}hendra_tariff_touring_extra
                UNION ALL
                SELECT code FROM {$prefix}hendra_tariff_touring_person_type
                UNION ALL
                SELECT code FROM {$prefix}hendra_tariff_touring_pitch_type
            ) a
            where code = '$pitch_type_code'
        ");
        if (count($result) > 0) {
            return array(
                'success' => false,
                'message' => 'An extra, pitch type or person type is already using this code.',
            );
        }
    
        // Run the query. It returns the amount of rows inserted.
        $amount = $wpdb->insert("{$prefix}hendra_tariff_touring_pitch_type", array(
            'name' => $pitch_type_name,
            'code' => $pitch_type_code,
        ));
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Pitch Type added successfully.',
                'model' => array(
                    'id' => $wpdb->insert_id,
                    'name' => $pitch_type_name,
                    'code' => $pitch_type_code,
                ),
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Unable to add Pitch Type.',
                'model' => null,
            );
        }

        // Return response
        return $response;
    }


    public function update ($pitch_type) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference range name
        $pitch_type_id = (int) $pitch_type['id'];
        $pitch_type_name = trim($pitch_type['name']);
        $pitch_type_code = trim($pitch_type['code']);
    
        // If model name is empty
        if ($pitch_type_name == '' || $pitch_type_code == '') {
            return array(
                'success' => false,
                'message' => 'A pitch type requires a name and a code.',
            );
        }

        // Check if an extra, person type or pitch type with this code already exists.
        $result = $wpdb->get_results("
            SELECT 
                1 
            FROM (
                SELECT code AS code FROM {$prefix}hendra_tariff_touring_extra
                UNION ALL
                SELECT code FROM {$prefix}hendra_tariff_touring_person_type
                UNION ALL
                SELECT code FROM {$prefix}hendra_tariff_touring_pitch_type WHERE id != '$pitch_type_id'
            ) a
            where code = '$pitch_type_code'
        ");
        if (count($result) > 0) {
            return array(
                'success' => false,
                'message' => 'An extra, pitch type or person type is already using this code.',
            );
        }

        // Run the query. It returns the amount of rows updates.
        $amount = $wpdb->update("{$prefix}hendra_tariff_touring_pitch_type", 
            array(
                'name' => $pitch_type_name,
                'code' => $pitch_type_code,
            ),
            array('id' => $pitch_type_id)
        );
    
        // If a row was inserted, return success
        if ($amount !== false) {
            $response = array(
                'success' => true,
                'message' => 'Pitch Type updated successfully.',
                'model' => array(
                    'id' => $pitch_type_id,
                    'name' => $pitch_type_name,
                    'code' => $pitch_type_code,
                ),
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Pitch Type with id ' . $pitch_type_id . ' was not found.',
                'model' => null,
            );
        }

        // Return response
        return $response;
    }


    public function delete ($pitch_type_id) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference input
        $pitch_type_id = (int) $pitch_type_id;
    
        // Run the query. It returns the amount of rows deleted.
        $amount = $wpdb->delete("{$prefix}hendra_tariff_touring_pitch_type", array(
            'id' => $pitch_type_id
        ));
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Pitch Type deleted successfully.',
                'id' => $range_id,
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Pitch Type with id ' . $pitch_type_id . ' was not found.',
                'model' => null,
            );
        }
    
        // Return response
        return $response;
    }


    public function registerAjax () {
        add_action("wp_ajax_hendra_tariff_get_pitch_types", function () {
            $response = $this->get();
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_create_pitch_type', function () {
            $pitch_type = $_POST['args']['pitch_type'];
            $response = $this->create($pitch_type);
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_update_pitch_type', function () {
            $pitch_type = $_POST['args']['pitch_type'];
            $response = $this->update($pitch_type);
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_delete_pitch_type', function () {
            $id = $_POST['args']['pitch_type_id'];
            $response = $this->delete($id);
            echo json_encode($response);
            exit;
        });
    }

}