<?php 

class HolidayHomesTariffDAO {

    public function import ($rows) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Now get all Models from DB
        $models = $wpdb->get_results("select id, name FROM {$prefix}hendra_tariff_static_model");
        
        // Extract just the names from the models.
        $models_names = array_map(function ($model) { return $model->name; }, $models);

        // Set up an array of invalid model names
        $invalid_model_names = array();

        // Set up an array of valud insert statements
        $insert_values = array();

        // Now loop through rows...
        foreach ($rows as $row) {

            // Reference the model name
            $model_name = $row['model_name'];

            // If the model name doesn't exist in the DB
            if (!in_array($model_name, $models_names)) {

                // And if we don't already know that this model doesn't exist,
                // Add it to the invalid model names array so we can return it later.
                if (!in_array($model_name, $invalid_model_names)) {
                    array_push($invalid_model_names, $model_name);
                }

            // Else the model name does exist in the DB
            } else {

                // Convert date from dd/mm/yyyy to yyyy-mm-dd 
                $arrival_date = DateTime::createFromFormat('d/m/Y', $row['arrival_date']);
				if ($arrival_date === false) {
                    return array (
                        'success' => false,
                        'message' => 'Error importing file. Invalid date format: ' . $row['arrival_date'],
                    );
				}
                $arrival_date = $arrival_date->format('Y-m-d');
                $arrival_date = $arrival_date . ' 00:00:00';
                $fully_booked = false;
                if (strtolower($row['fully_booked']) == 'yes') {
                    $fully_booked = true;
                }
                if ($row['fully_booked'] == '1' || $row['fully_booked'] == 1) {
                    $fully_booked = true;
                }
                if (strtolower($row['fully_booked']) == 'true') {
                    $fully_booked = true;
                }

                // Add another insert statement to the insert query we're building
                array_push($insert_values, '(
                    (SELECT id FROM ' . $prefix . 'hendra_tariff_static_model WHERE name = "' . $model_name . '" LIMIT 1),
                    "'. $arrival_date . '", 
                    ' . (int) $row['duration'] . ', 
                    ' . $row['price'] . ', 
                    ' . $row['discount'] . ',
                    ' . (int) $fully_booked . '
                )');

            }
        }

        // Now, if the invalid model names array contains any elements,
        // there was an error so return success as false etc.
        if (count($invalid_model_names) > 0) {
            return array (
                'success' => false,
                'message' => 'Error importing file. Model codes ' . implode(", ", $invalid_model_names) . ' were not found in the database.',
            );
        }

        // Now everythings all good, delete the current rows from calendar
        $wpdb->query("DELETE FROM {$prefix}hendra_tariff_calendar_statics");

        // Create the insert query for the calendar, using the query we were building earlier
        $insert_query = "INSERT INTO {$prefix}hendra_tariff_calendar_statics (static_model_id, arrival_date, duration, price, discount, fully_booked) VALUES ";
        $insert_query .= implode( ",\n", $insert_values);

        // Run the insert query. It returns the amount of rows inserted
        $insert_query_result = $wpdb->query($insert_query);

        // Return result
        return array(
            'success' => true,
            'message' => 'Successfully imported ' . $insert_query_result . '/' . count($rows) . ' rows.',
        );

    }

    public function export () {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $rows = $wpdb->get_results("
            SELECT 
                DATE_FORMAT(CAST({$prefix}hendra_tariff_calendar_statics.arrival_date as DATE), '%d/%m/%Y') as arrival_date,
                {$prefix}hendra_tariff_calendar_statics.duration,
                {$prefix}hendra_tariff_static_model.name as model_name,
                {$prefix}hendra_tariff_calendar_statics.price,
                {$prefix}hendra_tariff_calendar_statics.discount,
                {$prefix}hendra_tariff_calendar_statics.fully_booked,
                CASE WHEN {$prefix}hendra_tariff_calendar_statics.fully_booked = 0 THEN 'NO' ELSE 'YES' END as fully_booked
            FROM 
                {$prefix}hendra_tariff_calendar_statics
            LEFT JOIN
                {$prefix}hendra_tariff_static_model
            ON 
                {$prefix}hendra_tariff_calendar_statics.static_model_id = {$prefix}hendra_tariff_static_model.id
        ");
        return array(
            'success' => true,
            'message' => 'Exported ' . count($rows) .  ' rows.',
            'data' => $rows,
        );
    }

    public function clear () {
        global $wpdb;
        $prefix = $wpdb->prefix;
        $result = $wpdb->query("DELETE FROM {$prefix}hendra_tariff_calendar_statics");
        return array(
            'success' => true,
            'message' => 'Deleted ' . $result . ' rows.',
        );
    }

    public function getByYearMonth($year, $month) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $rows = $wpdb->get_results("
            SELECT 
                DATE_FORMAT(CAST(cal.arrival_date as DATE), '%d/%m/%Y') as arrival_date,
                cal.duration as duration,
                models.name as model_name,
                statics.name as static_name,
                statics.flash as static_flash,
                ranges.name as range_name,
                cal.price as price,
                cal.discount as discount,
                cal.fully_booked as fully_booked
            FROM 
                {$prefix}hendra_tariff_calendar_statics as cal
            LEFT JOIN
                {$prefix}hendra_tariff_static_model as models
            ON 
                cal.static_model_id = models.id
            LEFT JOIN
                {$prefix}hendra_tariff_static as statics
            ON 
                models.static_id = statics.id
            LEFT JOIN
                {$prefix}hendra_tariff_static_range as ranges
            ON 
                statics.range_id = ranges.id
            WHERE 
				YEAR(cal.arrival_date) >= $year
            AND
                MONTH(cal.arrival_date) = $month
            ORDER BY 
                duration DESC, arrival_date ASC, ranges.order ASC, statics.order ASC, models.name ASC
            ");
        return array(
            'success' => true,
            'message' => 'Retrieved Holiday Homes Tariff successfully.',
            'data' => $rows,
        );
    }

    public function getCount () {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $result = $wpdb->get_results("SELECT COUNT(*) as amount FROM {$prefix}hendra_tariff_calendar_statics");
        return $result[0]->amount;
    }

    public function getExample () {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Create the array to return
        $return = array();

        // Append the registered person types
        $models = $wpdb->get_results("SELECT name FROM {$prefix}hendra_tariff_static_model");
        foreach ($models as $model) {
            array_push($return, array(
                'arrival_date' => '27/03/2020',
                'duration' => 3,
                'model_name' => $model->name,
                'price' => mt_rand(100*10, 1000*10) / 10,
                'discount' => 0,
                'fully_booked' => 'NO',
            ));
        }
        array_push($return, array(
            'arrival_date' => '',
            'duration' => '',
            'model_name' => '',
            'price' => '',
            'discount' => '',
            'fully_booked' => '',
        ));
        foreach ($models as $model) {
            array_push($return, array(
                'arrival_date' => '28/03/2020',
                'duration' => 7,
                'model_name' => $model->name,
                'price' => mt_rand(100*10, 1000*10) / 10,
                'discount' => 0,
                'fully_booked' => 'NO',
            ));
        }
        array_push($return, array(
            'arrival_date' => '',
            'duration' => '',
            'model_name' => '',
            'price' => '',
            'discount' => '',
            'fully_booked' => '',
        ));
        foreach ($models as $model) {
            array_push($return, array(
                'arrival_date' => '30/03/2020',
                'duration' => 4,
                'model_name' => $model->name,
                'price' => mt_rand(100*10, 1000*10) / 10,
                'discount' => 0,
                'fully_booked' => 'NO',
            ));
        }
        return $return;
    }

    public function registerAjax () {
        add_action("wp_ajax_hendra_tariff_holiday_homes_import", function () {
            $rows = $_POST['args']['rows'];
            $response = $this->import($rows);
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_holiday_homes_export", function () {
            $response = $this->export();
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_holiday_homes_clear", function () {
            $response = $this->clear();
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_holiday_homes_get_by_year_month", function () {
            $month = (int) $_POST['args']['month'];
            $year = (int) $_POST['args']['year'];
            $response = $this->getByYearMonth($year, $month);
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_nopriv_hendra_tariff_holiday_homes_get_by_year_month", function () {
            $month = (int) $_POST['args']['month'];
            $year = (int) $_POST['args']['year'];
            $response = $this->getByYearMonth($year, $month);
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_holiday_homes_get_example", function () {
            $response = $this->getExample();
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_holiday_homes_get_count", function () {
            $response = $this->getCount();
            echo json_encode($response);
            exit;
        });
    }


}

