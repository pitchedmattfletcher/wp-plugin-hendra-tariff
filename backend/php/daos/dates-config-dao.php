<?php 

class DatesConfigDAO {

    public function get () {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $response = array(
            'success' => true,
            'message' => 'Retrieved Dates successfully.',
            'data' => array()
        );
        $result = $wpdb->get_results("
            SELECT 
                {$prefix}hendra_tariff_dates_config.id, 
                {$prefix}hendra_tariff_dates_config.month,
                {$prefix}hendra_tariff_dates_config.year
            FROM 
                {$prefix}hendra_tariff_dates_config
            ORDER BY 
                {$prefix}hendra_tariff_dates_config.year,
                {$prefix}hendra_tariff_dates_config.month
            ASC
        ");
        $response['data'] = $result;
        return $response;
    }


    public function create ($date_config) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference range name
        $month = trim($date_config['month']);
        $year = trim($date_config['year']);
    
        // If model name is empty
        if ($month == '' || $year == '') {
            return array(
                'success' => false,
                'message' => 'A date requires a month and a year.',
            );
        }
    
        // Run the query. It returns the amount of rows inserted.
        $amount = $wpdb->insert("{$prefix}hendra_tariff_dates_config", array(
            'month' => $month,
            'year' => $year,
        ));
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Date added successfully.',
                'model' => array(
                    'id' => $wpdb->insert_id,
                    'month' => $month,
                    'year' => $year,
                ),
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Unable to add Date.',
                'model' => null,
            );
        }

        // Return response
        return $response;
    }


    public function delete ($date_id) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference input
        $date_id = (int) $date_id;
    
        // Run the query. It returns the amount of rows deleted.
        $amount = $wpdb->delete("{$prefix}hendra_tariff_dates_config", array(
            'id' => $date_id
        ));
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Date deleted successfully.',
                'id' => $date_id,
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Date with id ' . $date_id . ' was not found.',
                'model' => null,
            );
        }
    
        // Return response
        return $response;
    }


    public function registerAjax () {
        add_action("wp_ajax_hendra_tariff_get_dates_config", function () {
            $response = $this->get();
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_nopriv_hendra_tariff_get_dates_config", function () {
            $response = $this->get();
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_create_date_config', function () {
            $date_config = $_POST['args']['date_config'];
            $response = $this->create($date_config);
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_delete_date_config', function () {
            $id = $_POST['args']['date_config_id'];
            $response = $this->delete($id);
            echo json_encode($response);
            exit;
        });
    }

}