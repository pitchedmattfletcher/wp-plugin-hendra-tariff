<?php 

class GlampingTariffDAO {

    public function import ($rows) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Now get all pod types from the db
        $codes = $wpdb->get_results("SELECT id, code FROM {$prefix}hendra_tariff_glamping_pod");
        
        // Map codes to new format.
        $codes = array_map(function ($code) { 
            return array(
                'id' => $code->id,
                'code' => $code->code,
            ); 
        }, $codes);

        // Set up an array of invalid codes
        $invalid_codes = array();

        // Set up an array of value insert statements
        $insert_values = array();

        // Now loop through rows...
        foreach ($rows as $row) {

            // Reference the model name
            $code = $row['code'];

            // If the code doesn't exist in the DB
            if (!in_array($code, array_map(function ($code) { return $code['code']; }, $codes))) {

                // And if we don't already know that this code doesn't exist,
                // Add it to the invalid codes array so we can return it later.
                if (!in_array($code, $invalid_codes)) {
                    array_push($invalid_codes, $code);
                }

            // Else the code does exist in the DB
            } else {

                // Convert date from dd/mm/yyyy to yyyy-mm-dd 
                $arrival_date = DateTime::createFromFormat('d/m/Y', $row['arrival_date']);
                if ($arrival_date === false) {
                    return array (
                        'success' => false,
                        'message' => 'Error importing file. Invalid date format: ' . $row['arrival_date'],
                    );
				}
                $arrival_date = $arrival_date->format('Y-m-d');
                $arrival_date = $arrival_date . ' 00:00:00';

                // Get code id and type
                $code_id = false;
                for ($i = 0; $i < count($codes); $i++) {
                    if ($codes[$i]['code'] == $code) {
                        $code_id = $codes[$i]['id'];
                        break;
                    }
                }

                // If code type was found
                if ($code_id != false) {
                    array_push($insert_values, '(' . (int) $code_id . ', "'. $arrival_date . '", 1, ' . $row['price'] . ', ' . $row['discount'] . ')');
                } 
 
            }
        }

        // Now, if the invalid model names array contains any elements,
        // there was an error so return success as false etc.
        if (count($invalid_codes) > 0) {
            return array (
                'success' => false,
                'message' => 'Error importing file. Codes ' . implode(", ", $invalid_codes) . ' were not found in the database.',
            );
        }

        // Now everythings all good, delete the current rows from calendar
        $wpdb->query("DELETE FROM {$prefix}hendra_tariff_glamping_calendar");

        // Create the insert query for the calendar, using the query we were building earlier
        $insert_query = "INSERT INTO {$prefix}hendra_tariff_glamping_calendar (glamping_pod_id, arrival_date, duration, price, discount) VALUES ";
        $insert_query .= implode( ",\n", $insert_values);

        // Run the insert query. It returns the amount of rows inserted
        $insert_query_result = $wpdb->query($insert_query);

        // Return result
        return array(
            'success' => true,
            'message' => 'Successfully imported ' . $insert_query_result . '/' . count($rows) . ' rows.',
        );

    }
    
    public function export () {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $rows = $wpdb->get_results("
            SELECT
                DATE_FORMAT(CAST(glamping_cal.arrival_date as DATE), '%d/%m/%Y') as arrival_date,
                pods.code as code,
                glamping_cal.price as price,
                glamping_cal.discount as discount
            FROM 
                {$prefix}hendra_tariff_glamping_calendar as glamping_cal
            LEFT JOIN
                {$prefix}hendra_tariff_glamping_pod as pods
            ON 
                glamping_cal.glamping_pod_id = pods.id
            ORDER BY
                glamping_cal.arrival_date ASC, pods.order
        ");
        return array(
            'success' => true,
            'message' => 'Exported ' . count($rows) .  ' rows.',
            'data' => $rows,
        );
    }

    public function clear () {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $result = $wpdb->query('DELETE FROM {$prefix}hendra_tariff_glamping_calendar');
        return array(
            'success' => true,
            'message' => 'Deleted ' . $result . ' rows.',
        );
    }

    public function getByYearMonth($year, $month) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $rows = $wpdb->get_results("
            SELECT
                DATE_FORMAT(CAST(glamping_cal.arrival_date as DATE), '%d/%m/%Y') as arrival_date,
                pods.name as name,
                pods.code as code,
                glamping_cal.price as price,
                glamping_cal.discount as discount
                
            FROM 
                {$prefix}hendra_tariff_glamping_calendar as glamping_cal
            LEFT JOIN
                {$prefix}hendra_tariff_glamping_pod as pods
            ON 
                glamping_cal.glamping_pod_id = pods.id
            WHERE 
                YEAR(glamping_cal.arrival_date) >= $year
            AND 
                MONTH(glamping_cal.arrival_date) = $month
            ORDER BY
                glamping_cal.arrival_date ASC, pods.order        
        ");
        return array(
            'success' => true,
            'message' => 'Retrieved Glamping Tariff successfully.',
            'data' => $rows,
        );
    }

    public function getCount () {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $result = $wpdb->get_results("SELECT COUNT(*) as amount FROM {$prefix}hendra_tariff_glamping_calendar");
        return $result[0]->amount;
    }

    public function getExample () {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Create the array to return
        $return = array();

        // Append the registered person types
        $pods = $wpdb->get_results("SELECT code as code FROM {$prefix}hendra_tariff_glamping_pod");
        foreach ($pods as $pod) {
            array_push($return, array(
                'arrival_date' => '02/05/2020',
                'code' => $pod->code,
                'price' => mt_rand (10*10, 100*10) / 10,
                'discount' => 0,
            ));
        }
        array_push($return, array(
            'arrival_date' => '',
            'code' => '',
            'price' => '',
            'discount' => '',
        ));
        foreach ($pods as $pod) {
            array_push($return, array(
                'arrival_date' => '09/05/2020',
                'code' => $pod->code,
                'price' => mt_rand (10*10, 100*10) / 10,
                'discount' => 0,
            ));
        }

        return $return;
    }

    public function registerAjax () {
        add_action("wp_ajax_hendra_tariff_glamping_import", function () {
            $rows = $_POST['args']['rows'];
            $response = $this->import($rows);
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_glamping_export", function () {
            $response = $this->export();
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_glamping_clear", function () {
            $response = $this->clear();
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_glamping_get_by_year_month", function () {
            $year = (int) $_POST['args']['year'];
            $month = (int) $_POST['args']['month'];
            $response = $this->getByYearMonth($year, $month);
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_nopriv_hendra_tariff_glamping_get_by_year_month", function () {
            $year = (int) $_POST['args']['year'];
            $month = (int) $_POST['args']['month'];
            $response = $this->getByYearMonth($year, $month);
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_glamping_get_example", function () {
            $response = $this->getExample();
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_glamping_get_count", function () {
            $response = $this->getCount();
            echo json_encode($response);
            exit;
        });
    }


}
