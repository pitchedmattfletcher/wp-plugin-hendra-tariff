<?php 

class PersonTypesDAO {

    public function get () {
        global $wpdb;
        $prefix = $wpdb->prefix;
        $response = array(
            'success' => true,
            'message' => 'Retrieved Person Types successfully.',
            'data' => array()
        );
        $result = $wpdb->get_results("
            SELECT 
                {$prefix}hendra_tariff_touring_person_type.id, 
                {$prefix}hendra_tariff_touring_person_type.name,
                {$prefix}hendra_tariff_touring_person_type.code
            FROM 
                {$prefix}hendra_tariff_touring_person_type
            ORDER BY 
                {$prefix}hendra_tariff_touring_person_type.id
            ASC
        ");
        $response['data'] = $result;
        return $response;
    }


    public function create ($personType) {
        global $wpdb;
        $prefix = $wpdb->prefix;


        // Reference range name
        $person_type_name = trim($personType['name']);
        $person_type_code = trim($personType['code']);
    
        // If model name is empty
        if ($person_type_name == '' || $person_type_code == '') {
            return array(
                'success' => false,
                'message' => 'A person type requires a name and a code.',
            );
        }

        // Check if an extra, person type or pitch type with this code already exists.
        $result = $wpdb->get_results("
            SELECT 
                1 
            FROM (
                SELECT code AS code FROM {$prefix}hendra_tariff_touring_extra
                UNION ALL
                SELECT code FROM {$prefix}hendra_tariff_touring_person_type
                UNION ALL
                SELECT code FROM {$prefix}hendra_tariff_touring_pitch_type
            ) a
            where code = '$person_type_code'
        ");
        if (count($result) > 0) {
            return array(
                'success' => false,
                'message' => 'An extra, pitch type or person type is already using this code.',
            );
        }
    
        // Run the query. It returns the amount of rows inserted.
        $amount = $wpdb->insert("{$prefix}hendra_tariff_touring_person_type", array(
            'name' => $person_type_name,
            'code' => $person_type_code,
        ));
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Person Type added successfully.',
                'model' => array(
                    'id' => $wpdb->insert_id,
                    'name' => $person_type_name,
                    'code' => $person_type_code,
                ),
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Unable to add Person Type.',
                'model' => null,
            );
        }

        // Return response
        return $response;
    }


    public function update ($personType) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference range name
        $person_type_id = (int) $personType['id'];
        $person_type_name = trim($personType['name']);
        $person_type_code = trim($personType['code']);
    
        // If model name is empty
        if ($person_type_name == '' || $person_type_code == '') {
            return array(
                'success' => false,
                'message' => 'A person type requires a name and a code.',
            );
        }

        // Check if an extra, person type or pitch type with this code already exists.
        $result = $wpdb->get_results("
            SELECT 
                1 
            FROM (
                SELECT code AS code FROM {$prefix}hendra_tariff_touring_extra
                UNION ALL
                SELECT code FROM {$prefix}hendra_tariff_touring_person_type WHERE id != '$person_type_id'
                UNION ALL
                SELECT code FROM {$prefix}hendra_tariff_touring_pitch_type
            ) a
            where code = '$person_type_code'
        ");
        if (count($result) > 0) {
            return array(
                'success' => false,
                'message' => 'An extra, pitch type or person type is already using this code.',
            );
        }

        // Run the query. It returns the amount of rows updates.
        $amount = $wpdb->update("{$prefix}hendra_tariff_touring_person_type", 
            array(
                'name' => $person_type_name,
                'code' => $person_type_code,
            ),
            array('id' => $person_type_id)
        );
    
        // If a row was inserted, return success
        if ($amount !== false) {
            $response = array(
                'success' => true,
                'message' => 'Person Type updated successfully.',
                'model' => array(
                    'id' => $person_type_id,
                    'name' => $person_type_name,
                    'code' => $person_type_code,
                ),
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Person Type with id ' . $person_type_id . ' was not found.',
                'model' => null,
            );
        }

        // Return response
        return $response;
    }


    public function delete ($person_type_id) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference input
        $person_type_id = (int) $person_type_id;
    
        // Run the query. It returns the amount of rows deleted.
        $amount = $wpdb->delete("{$prefix}hendra_tariff_touring_person_type", array(
            'id' => $person_type_id
        ));
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Person Type deleted successfully.',
                'id' => $range_id,
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Person Type with id ' . $person_type_id . ' was not found.',
                'model' => null,
            );
        }
    
        // Return response
        return $response;
    }


    public function registerAjax () {
        add_action("wp_ajax_hendra_tariff_get_person_types", function () {
            $response = $this->get();
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_create_person_type', function () {
            $personType = $_POST['args']['person_type'];
            $response = $this->create($personType);
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_update_person_type', function () {
            $personType = $_POST['args']['person_type'];
            $response = $this->update($personType);
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_delete_person_type', function () {
            $id = $_POST['args']['person_type_id'];
            $response = $this->delete($id);
            echo json_encode($response);
            exit;
        });
    }

}