<?php 

class GlampingPodsDAO {

    public function get () {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $response = array(
            'success' => true,
            'message' => 'Retrieved Pods successfully.',
            'data' => array()
        );
        $result = $wpdb->get_results("
            SELECT 
                {$prefix}hendra_tariff_glamping_pod.id, 
                {$prefix}hendra_tariff_glamping_pod.name,
                {$prefix}hendra_tariff_glamping_pod.code,
                {$prefix}hendra_tariff_glamping_pod.order
            FROM 
                {$prefix}hendra_tariff_glamping_pod
            ORDER BY 
                {$prefix}hendra_tariff_glamping_pod.order
            ASC
        ");
        $response['data'] = $result;
        return $response;
    }

    public function create ($pod) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $pod_name = trim($pod['name']);
        $pod_code = trim($pod['code']);
        $pod_order = (int) $pod['order'] ?? 0;
            
        // If model name is empty
        if ($pod_name == '' || $pod_code == '') {
            return array(
                'success' => false,
                'message' => 'A pod requires a name and a code.',
            );
        }

        // Check if a pod with this name already exists
        $result = $wpdb->get_results("SELECT name FROM {$prefix}hendra_tariff_glamping_pod WHERE name = '$pod_name' LIMIT 1");
        if (count($result) > 0) {
            return array(
                'success' => false,
                'message' => 'A pod with this name already exists.',
            );
        }
        
        // Check if a pod with this code already exists
        $result = $wpdb->get_results("SELECT code FROM {$prefix}hendra_tariff_glamping_pod WHERE code = '$pod_code' LIMIT 1");
        if (count($result) > 0) {
            return array(
                'success' => false,
                'message' => 'A pod with this code already exists.',
            );
        }

        // Run the query. It returns the amount of rows inserted.
        $amount = $wpdb->insert("{$prefix}hendra_tariff_glamping_pod", array(
            'name' => $pod_name,
            'code' => $pod_code,
            'order' => $pod_order
        ));
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Pod added successfully.',
                'model' => array(
                    'id' => $wpdb->insert_id,
                    'name' => $pod_name,
                    'code' => $pod_code,
                    'order' => $pod_order,
                ),
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Unable to add Pod.',
                'model' => null,
            );
        }

        // Return response
        return $response;

    }

    public function update ($pod) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $pod_id = (int) $pod['id'];
        $pod_name = trim($pod['name']);
        $pod_code = trim($pod['code']);
        $pod_order = (int) $pod['order'] ?? 0;
    
        // If model name is empty
        if ($pod_name == '' || $pod_code == '') {
            return array(
                'success' => false,
                'message' => 'A pod requires a name and a code.',
            );
        }

        // Check if a pod with this name already exists
        $result = $wpdb->get_results("SELECT name FROM {$prefix}hendra_tariff_glamping_pod WHERE name = '$pod_name' AND id != '$pod_id' LIMIT 1");
        if (count($result) > 0) {
            return array(
                'success' => false,
                'message' => 'A pod with this name already exists.',
            );
        }
        
        // Check if a pod with this code already exists
        $result = $wpdb->get_results("SELECT code FROM {$prefix}hendra_tariff_glamping_pod WHERE code = '$pod_code' AND id != '$pod_id' LIMIT 1");
        if (count($result) > 0) {
            return array(
                'success' => false,
                'message' => 'A pod with this code already exists.',
            );
        }

        // Run the query. It returns the amount of rows updates.
        $amount = $wpdb->update("{$prefix}hendra_tariff_glamping_pod", 
            array(
                'name' => $pod_name,
                'code' => $pod_code,
                'order' => $pod_order,
            ),
            array('id' => $pod_id)
        );
    
        // If a row was inserted, return success
        if ($amount !== false) {
            $response = array(
                'success' => true,
                'message' => 'Pod updated successfully.',
                'model' => array(
                    'id' => $pod_id,
                    'name' => $pod_name,
                    'code' => $pod_code,
                    'order' => $pod_order
                ),
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Pod with id ' . $pod_id . ' was not found.',
                'model' => null,
            );
        }

        // Return response
        return $response;
    }

    public function delete ($pod_id) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference input
        $pod_id = (int) $pod_id;
    
        // Run the query. It returns the amount of rows deleted.
        $amount = $wpdb->delete("{$prefix}hendra_tariff_glamping_pod", array(
            'id' => $pod_id
        ));
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Pod deleted successfully.',
                'id' => $range_id,
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Pod with id ' . $pod_id . ' was not found.',
                'model' => null,
            );
        }
    
        // Return response
        return $response;
    }

    public function registerAjax () {
        add_action("wp_ajax_hendra_tariff_get_pods", function () {
            $response = $this->get();
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_create_pod', function () {
            $extra = $_POST['args']['pod'];
            $response = $this->create($extra);
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_update_pod', function () {
            $extra = $_POST['args']['pod'];
            $response = $this->update($extra);
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_delete_pod', function () {
            $id = $_POST['args']['pod_id'];
            $response = $this->delete($id);
            echo json_encode($response);
            exit;
        });
    }

}