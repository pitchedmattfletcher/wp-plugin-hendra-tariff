<?php 

/**
 * Hendra Tariff Models DAO.
 */
class ModelsDAO {

    /**
     * Get all models
     */
    public function get () {
        global $wpdb;
        $prefix = $wpdb->prefix;
        $response = array(
            'success' => true,
            'message' => 'Retrieved Models successfully.',
            'data' => array()
        );
        $result = $wpdb->get_results("SELECT {$prefix}hendra_tariff_static_model.id, {$prefix}hendra_tariff_static_model.static_id, {$prefix}hendra_tariff_static_model.name FROM {$prefix}hendra_tariff_static_model");
        $response['data'] = $result;
        return $response;
    }

    /**
     * Get all models for a specific static
     */
    public function getByStaticId ($static_id) {
        global $wpdb;
        $prefix = $wpdb->prefix;
        $response = array(
            'success' => true,
            'message' => 'Retrieved Models successfully.',
            'data' => array()
        );
        $result = $wpdb->get_results("SELECT {$prefix}hendra_tariff_static_model.id, {$prefix}hendra_tariff_static_model.static_id, {$prefix}hendra_tariff_static_model.name FROM {$prefix}hendra_tariff_static_model WHERE static_id = $static_id");
        $response['data'] = $result;
        return $response;
    }

    /**
     * Create a model
     */
    public function create ($model) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference input
        $model_name = trim($model['name']);
        $model_static_id = (int) $model['static_id'];
    
        // If model name is empty
        if (!isset($model_name) || trim($model_name) === '') {
            return array(
                'success' => false,
                'message' => 'A model requires a name.',
            );
            exit;
        }
    
        // Check if a model with this name already exists
        $result = $wpdb->get_results("SELECT name FROM {$prefix}hendra_tariff_static_model WHERE name = '$model_name' LIMIT 1");
        if (count($result) > 0) {
            return array(
                'success' => false,
                'message' => 'A model with this name already exists.',
            );
            exit;
        }
    
        // Check if a static with this static_id actually exists
        $result = $wpdb->get_results("SELECT id FROM {$prefix}hendra_tariff_static WHERE id = $model_static_id LIMIT 1");
        if (count($result) == 0) {
            return array(
                'success' => false,
                'message' => 'The assigned Static does not exist.',
            );
            exit;
        }
    
        // Run the query. It returns the amount of rows inserted.
        $amount = $wpdb->insert("{$prefix}hendra_tariff_static_model", array(
            'name' => $model_name,
            'static_id' => $model_static_id,
        ));
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Model added successfully.',
                'model' => array(
                    'id' => $wpdb->insert_id,
                    'name' => $model_name,
                    'static_id' => $model_static_id,
                ),
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Unable to add Model.',
                'model' => null,
            );
        }
    
        return $response;
    }

    /**
     * Delete model by id
     */
    public function delete ($model_id) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $model_id = (int) $model_id;
        $amount = $wpdb->delete("{$prefix}hendra_tariff_static_model", array(
            'id' => $model_id
        ));
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Model deleted successfully.',
                'id' => $model_id,
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Unable to delete Model.',
                'model' => null,
            );
        }
        return $response;
    }

    /**
     * Register ajax for wordpress
     */
    public function registerAjax () {
        add_action("wp_ajax_hendra_tariff_get_models", function () {
            $response = $this->get();
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_get_models_by_static_id", function () {
            $static_id = $_POST['args']['static_id'];
            $response = $this->getByStaticId($static_id);
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_create_model", function () {
            $model = $_POST['args']['model'];
            $response = $this->create($model);
            echo json_encode($response);
            exit;
        });
        add_action("wp_ajax_hendra_tariff_delete_model", function () {
            $model_id = $_POST['args']['model_id'];
            $response = $this->delete($model_id);
            echo json_encode($response);
            exit;
        });
    }

}