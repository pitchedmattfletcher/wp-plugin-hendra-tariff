<?php 

class ExtrasDAO {

    public function get () {
        global $wpdb;
        $prefix = $wpdb->prefix;
        $response = array(
            'success' => true,
            'message' => 'Retrieved Extras successfully.',
            'data' => array()
        );
        $result = $wpdb->get_results("
            SELECT 
                {$prefix}hendra_tariff_touring_extra.id, 
                {$prefix}hendra_tariff_touring_extra.name,
                {$prefix}hendra_tariff_touring_extra.code
            FROM 
                {$prefix}hendra_tariff_touring_extra
            ORDER BY 
                {$prefix}hendra_tariff_touring_extra.id
            ASC
        ");
        $response['data'] = $result;
        return $response;
    }

    public function create ($extra) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $extra_name = trim($extra['name']);
        $extra_code = trim($extra['code']);
            
        // If model name is empty
        if ($extra_name == '' || $extra_code == '') {
            return array(
                'success' => false,
                'message' => 'An extra requires a name and a code.',
            );
        }

        // Check if an extra, person type or pitch type with this code already exists.
        $result = $wpdb->get_results("
            SELECT 
                1 
            FROM (
                SELECT code AS code FROM {$prefix}hendra_tariff_touring_extra
                UNION ALL
                SELECT code FROM {$prefix}hendra_tariff_touring_person_type
                UNION ALL
                SELECT code FROM {$prefix}hendra_tariff_touring_pitch_type
            ) a
            where code = '$extra_code'
        ");
        if (count($result) > 0) {
            return array(
                'success' => false,
                'message' => 'An extra, pitch type or person type is already using this code.',
            );
        }
            
        // Run the query. It returns the amount of rows inserted.
        $amount = $wpdb->insert("{$prefix}hendra_tariff_touring_extra", array(
            'name' => $extra_name,
            'code' => $extra_code,
        ));
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Extra added successfully.',
                'model' => array(
                    'id' => $wpdb->insert_id,
                    'name' => $extra_name,
                    'code' => $extra_code,
                ),
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Unable to add Extra.',
                'model' => null,
            );
        }

        // Return response
        return $response;

    }

    public function update ($extra) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference range name
        $extra_id = (int) $extra['id'];
        $extra_name = trim($extra['name']);
        $extra_code = trim($extra['code']);
    
        // If model name is empty
        if ($extra_name == '' || $extra_code == '') {
            return array(
                'success' => false,
                'message' => 'An extra requires a name and a code.',
            );
        }

        // Check if an extra, person type or pitch type with this code already exists.
        $result = $wpdb->get_results("
            SELECT 
                1 
            FROM (
                SELECT code AS code FROM {$prefix}hendra_tariff_touring_extra WHERE id != '$extra_id'
                UNION ALL
                SELECT code FROM {$prefix}hendra_tariff_touring_person_type
                UNION ALL
                SELECT code FROM {$prefix}hendra_tariff_touring_pitch_type
            ) a
            where code = '$extra_code'
        ");
        if (count($result) > 0) {
            return array(
                'success' => false,
                'message' => 'An extra, pitch type or person type is already using this code.',
            );
        }

        // Run the query. It returns the amount of rows updates.
        $amount = $wpdb->update("{$prefix}hendra_tariff_touring_extra", 
            array(
                'name' => $extra_name,
                'code' => $extra_code,
            ),
            array('id' => $extra_id)
        );
    
        // If a row was inserted, return success
        if ($amount !== false) {
            $response = array(
                'success' => true,
                'message' => 'Extra updated successfully.',
                'model' => array(
                    'id' => $extra_id,
                    'name' => $extra_name,
                    'code' => $extra_code,
                ),
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Extra with id ' . $extra_id . ' was not found.',
                'model' => null,
            );
        }

        // Return response
        return $response;
    }

    public function delete ($extra_id) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference input
        $extra_id = (int) $extra_id;
    
        // Run the query. It returns the amount of rows deleted.
        $amount = $wpdb->delete("{$prefix}hendra_tariff_touring_extra", array(
            'id' => $extra_id
        ));
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Extra deleted successfully.',
                'id' => $range_id,
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Extra with id ' . $extra_id . ' was not found.',
                'model' => null,
            );
        }
    
        // Return response
        return $response;
    }

    public function registerAjax () {
        add_action("wp_ajax_hendra_tariff_get_extras", function () {
            $response = $this->get();
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_create_extra', function () {
            $extra = $_POST['args']['extra'];
            $response = $this->create($extra);
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_update_extra', function () {
            $extra = $_POST['args']['extra'];
            $response = $this->update($extra);
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_delete_extra', function () {
            $id = $_POST['args']['extra_id'];
            $response = $this->delete($id);
            echo json_encode($response);
            exit;
        });
    }

}