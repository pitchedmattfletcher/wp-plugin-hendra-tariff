<?php 

/**
 * Hendra Tariff Ranges DAO.
 */
class RangesDAO {

    /**
     * Get all ranges
     */
    public function get () {
        global $wpdb;
        $prefix = $wpdb->prefix;

        $response = array(
            'success' => true,
            'message' => 'Retrieved Ranges successfully.',
            'data' => array()
        );
        $result = $wpdb->get_results("SELECT {$prefix}hendra_tariff_static_range.id, {$prefix}hendra_tariff_static_range.name, {$prefix}hendra_tariff_static_range.order FROM {$prefix}hendra_tariff_static_range ORDER BY {$prefix}hendra_tariff_static_range.order ASC");
        $response['data'] = $result;
        return $response;
    }

    /**
     * Create a range
     */
    public function create ($range) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference range name
        $range_name = trim($range['name']);
        $range_order = (int) $range['order'] ?? 0;
    
        // If model name is empty
        if (!isset($range_name) || trim($range_name) === '') {
            return array(
                'success' => false,
                'message' => 'A range requires a name.',
            );
        }

        // Check if a range with this name already exists
        $range_result = $wpdb->get_results("SELECT name FROM {$prefix}hendra_tariff_static_range WHERE name = '$range_name' LIMIT 1");
        if (count($range_result) > 0) {
            return array(
                'success' => false,
                'message' => 'A range with this name already exists.',
            );
        }
    
        // Run the query. It returns the amount of rows inserted.
        $amount = $wpdb->insert("{$prefix}hendra_tariff_static_range", array(
            'name' => $range_name,
            'order' => $range_order,
        ));
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Range added successfully.',
                'model' => array(
                    'id' => $wpdb->insert_id,
                    'name' => $range_name,
                ),
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Unable to add Range.',
                'model' => null,
            );
        }

        // Return response
        return $response;
    }

    /**
     * Update a range
     */
    public function update ($range) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference input
        $range_id = (int) $range['id'];
        $range_name = trim($range['name']);
        $range_order = (int) $range['order'] ?? 0;
    
        // If model name is empty
        if (!isset($range_name) || $range_name === '') {
            return array(
                'success' => false,
                'message' => 'A range requires a name.',
            );
            exit;
        }
    
        // Check if a range with this name already exists
        $range_result = $wpdb->get_results("SELECT name FROM {$prefix}hendra_tariff_static_range WHERE name = '$range_name' AND id != '$range_id' LIMIT 1");
        if (count($range_result) > 0) {
            return array(
                'success' => false,
                'message' => 'A range with this name already exists.',
            );
            exit;
        }
    
        // Run the query. It returns the amount of rows updates.
        $amount = $wpdb->update("{$prefix}hendra_tariff_static_range", 
            array(
                'name' => $range_name,
                'order' => $range_order,
            ),
            array('id' => $range_id)
        );
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Range updated successfully.',
                'model' => array(
                    'id' => $range_id,
                    'name' => $range_name,
                ),
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Range with id ' . $range_id . ' was not found.',
                'model' => null,
            );
        }

        // Return response
        return $response;
    }

    /**
     * Delete a range
     */
    public function delete ($range_id) {
        global $wpdb;
        $prefix = $wpdb->prefix;

        // Reference input
        $range_id = (int) $range_id;
    
        // Run the query. It returns the amount of rows deleted.
        $amount = $wpdb->delete("{$prefix}hendra_tariff_static_range", array(
            'id' => $range_id
        ));
    
        // If a row was inserted, return success
        if ($amount > 0) {
            $response = array(
                'success' => true,
                'message' => 'Range deleted successfully.',
                'id' => $range_id,
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Range with id ' . $range_id . ' was not found.',
                'model' => null,
            );
        }
    
        // Return response
        return $response;
    }

    /**
     * Register ajax with wordpress
     */
    public function registerAjax () {
        add_action("wp_ajax_hendra_tariff_get_ranges", function () {
            $response = $this->get();
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_create_range', function () {
            $range = $_POST['args']['range'];
            $response = $this->create($range);
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_update_range', function () {
            $range = $_POST['args']['range'];
            $response = $this->update($range);
            echo json_encode($response);
            exit;
        });
        add_action('wp_ajax_hendra_tariff_delete_range', function () {
            $range_id = $_POST['args']['range_id'];
            $response = $this->delete($range_id);
            echo json_encode($response);
            exit;
        });
    }

}