<?php 

class HolidayHomesCaravansPage {

    public function __construct() {
        $this->registerScripts();
        $this->createMenu();
    }

    public function registerScripts() {
        add_action('admin_enqueue_scripts', function(){
            if (isset($_GET['page']) && ($_GET['page'] == 'hendra-tariff-holiday-homes-caravans')) { 
                wp_enqueue_style('hendra-tariff-css', plugin_dir_url( __DIR__ ) . '../css/style.min.css');
                wp_enqueue_script('hendra-tariff-notification', plugin_dir_url(__DIR__) . '../js/plugins/notification.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-tablefy', plugin_dir_url(__DIR__) . '../js/plugins/tablefy.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-form-modal', plugin_dir_url(__DIR__) . '../js/plugins/form-modal.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-ranges-dao', plugin_dir_url(__DIR__) . '../js/daos/ranges-dao.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-statics-dao', plugin_dir_url(__DIR__) . '../js/daos/statics-dao.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-models-dao', plugin_dir_url(__DIR__) . '../js/daos/models-dao.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-holiday-homes-caravans-page-template', plugin_dir_url(__DIR__) . '../js/pages/holiday-homes-caravans-page-template.js', array('jquery', 'hendra-tariff-ranges-dao', 'hendra-tariff-statics-dao', 'hendra-tariff-models-dao', 'hendra-tariff-form-modal', 'hendra-tariff-tablefy'), 1.0, true);
            }
        });
    }

    public function createMenu () {
        add_action('admin_menu', function(){
            add_submenu_page('hendra-tariff', '&#8226; Caravans', '&#8226; Caravans', 'manage_options', 'hendra-tariff-holiday-homes-caravans', array($this, 'render'));
        });
    }

    public function render () {
        include(plugin_dir_path( __DIR__ ) . '/templates/holiday-homes-caravans-page-template.php');
    }
}