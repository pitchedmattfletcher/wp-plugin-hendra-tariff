<?php 

class TouringImportPage {

    public function __construct() {
        $this->registerScripts();
        $this->createMenu();
    }

    public function registerScripts() {
        add_action('admin_enqueue_scripts', function(){
            if (isset($_GET['page']) && ($_GET['page'] == 'hendra-tariff-touring-import')) { 
                wp_enqueue_style('hendra-tariff-css', plugin_dir_url(__DIR__) . '../css/style.min.css');
                wp_enqueue_script('hendra-tariff-notification', plugin_dir_url(__DIR__) . '../js/plugins/notification.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-tablefy', plugin_dir_url(__DIR__) . '../js/plugins/tablefy.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-form-modal', plugin_dir_url(__DIR__) . '../js/plugins/form-modal.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-touring-tariff-dao', plugin_dir_url(__DIR__) . '../js/daos/touring-tariff-dao.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-papaparse', plugin_dir_url(__DIR__) . '../js/plugins/papaparse.min.js', array(), 1.0, true);
                wp_enqueue_script('hendra-tariff-touring-import-page-template', plugin_dir_url(__DIR__) . '../js/pages/touring-import-page-template.js', array('jquery'), 1.0, true);
            }
        });
    }

    public function createMenu () {
        add_action('admin_menu', function(){
            add_submenu_page('hendra-tariff', '&#8226; Import / Export', '&#8226; Import / Export', 'manage_options', 'hendra-tariff-touring-import', array($this, 'render'));
        });
    }

    public function render () {
        include(plugin_dir_path( __DIR__ ) . '/templates/touring-import-page-template.php');
    }
}