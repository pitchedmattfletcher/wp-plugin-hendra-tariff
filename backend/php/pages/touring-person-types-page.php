<?php 

class TouringPersonTypesPage {

    public function __construct() {
        $this->registerScripts();
        $this->createMenu();
    }

    public function registerScripts() {
        add_action('admin_enqueue_scripts', function(){
            if (isset($_GET['page']) && ($_GET['page'] == 'hendra-tariff-touring-person-types')) { 
                wp_enqueue_style('hendra-tariff-css', plugin_dir_url( __DIR__ ) . '../css/style.min.css');
                wp_enqueue_script('hendra-tariff-notification', plugin_dir_url(__DIR__) . '../js/plugins/notification.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-tablefy', plugin_dir_url(__DIR__) . '../js/plugins/tablefy.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-form-modal', plugin_dir_url(__DIR__) . '../js/plugins/form-modal.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-person-types-dao', plugin_dir_url(__DIR__) . '../js/daos/person-types-dao.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-touring-person-types-page-template', plugin_dir_url(__DIR__) . '../js/pages/touring-person-types-page-template.js', array('jquery', 'hendra-tariff-form-modal', 'hendra-tariff-tablefy'), 1.0, true);
            }
        });
    }

    public function createMenu () {
        add_action('admin_menu', function(){
            add_submenu_page('hendra-tariff', 'Touring', 'Touring', 'manage_options', 'hendra-tariff-touring-person-types', array($this, 'render'));
            add_submenu_page('hendra-tariff', '&#8226; Person Types', '&#8226; Person Types', 'manage_options', 'hendra-tariff-touring-person-types', array($this, 'render'));
        });
    }

    public function render () {
        include(plugin_dir_path( __DIR__ ) . '/templates/touring-person-types-page-template.php');
    }
}