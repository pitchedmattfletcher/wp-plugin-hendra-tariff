<?php 

class GlampingPodsPage {

    public function __construct() {
        $this->registerScripts();
        $this->createMenu();
    }

    public function registerScripts() {
        add_action('admin_enqueue_scripts', function(){
            if (isset($_GET['page']) && ($_GET['page'] == 'hendra-tariff-glamping-pods')) { 
                wp_enqueue_style('hendra-tariff-css', plugin_dir_url( __DIR__ ) . '../css/style.min.css');
                wp_enqueue_script('hendra-tariff-notification', plugin_dir_url(__DIR__) . '../js/plugins/notification.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-tablefy', plugin_dir_url(__DIR__) . '../js/plugins/tablefy.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-form-modal', plugin_dir_url(__DIR__) . '../js/plugins/form-modal.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-glamping-pods-dao', plugin_dir_url(__DIR__) . '../js/daos/glamping-pods-dao.js', array('jquery'), 1.0, true);
                wp_enqueue_script('hendra-tariff-glamping-pods-page-template', plugin_dir_url(__DIR__) . '../js/pages/glamping-pods-page-template.js', array('jquery', 'hendra-tariff-form-modal', 'hendra-tariff-tablefy'), 1.0, true);
            }
        });
    }

    public function createMenu () {
        add_action('admin_menu', function(){
            add_submenu_page('hendra-tariff', 'Glamping', 'Glamping', 'manage_options', 'hendra-tariff-glamping-pods', array($this, 'render'));
            add_submenu_page('hendra-tariff', '&#8226; Pods', '&#8226; Pods', 'manage_options', 'hendra-tariff-glamping-pods', array($this, 'render'));
        });
    }

    public function render () {
        include(plugin_dir_path( __DIR__ ) . '/templates/glamping-pods-page-template.php');
    }
}