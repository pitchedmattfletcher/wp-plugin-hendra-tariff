<?php 

class HendraTariffDB {

    public function create () {
        $this->createHolidayHomes();
        $this->createTouring();
        $this->createGlamping();
        $this->createDatesConfig();
        $this->alterHolidayHomes();
    }

    private function createHolidayHomes () {
        global $wpdb;
        $prefix = $wpdb->prefix;
        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$prefix}hendra_tariff_static_range` (
                `id` int NOT NULL AUTO_INCREMENT,
                `name` varchar(255) NOT NULL,
                `order` int NOT NULL DEFAULT 0,
                `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `date_updated` timestamp,
                PRIMARY KEY (id)
            );
        ");
        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$prefix}hendra_tariff_static` (
                `id` int NOT NULL AUTO_INCREMENT,
                `range_id` int NOT NULL,
                `name` varchar(255) NOT NULL,
                `flash` varchar(255),
                `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `date_updated` timestamp,
                PRIMARY KEY (id),
                FOREIGN KEY (range_id) REFERENCES {$prefix}hendra_tariff_static_range(id) ON DELETE CASCADE
            );
        ");
        $wpdb->query("
            CREATE TABLE IF NOT EXISTS {$prefix}hendra_tariff_static_model (
                id INT NOT NULL AUTO_INCREMENT,
                static_id int NOT NULL,
                name varchar(255) NOT NULL,
                date_created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                date_updated timestamp,
                PRIMARY KEY (id),
                FOREIGN KEY (static_id) REFERENCES {$prefix}hendra_tariff_static(id) ON DELETE CASCADE
            );
        ");
        $wpdb->query("
            CREATE TABLE IF NOT EXISTS {$prefix}hendra_tariff_calendar_statics (
                id INT NOT NULL AUTO_INCREMENT,
                static_model_id INT NOT NULL,
                arrival_date datetime NOT NULL,
                duration INT NOT NULL,
                price DECIMAL(13, 2) NOT NULL,
                discount DECIMAL(13, 2) NOT NULL,
                PRIMARY KEY (id),
                FOREIGN KEY (static_model_id) REFERENCES {$prefix}hendra_tariff_static_model(id) ON DELETE CASCADE
            );
        ");
    }

    private function createTouring () {
        global $wpdb;
        $prefix = $wpdb->prefix;
        $wpdb->query("
            CREATE TABLE IF NOT EXISTS {$prefix}hendra_tariff_touring_extra (
                id int NOT NULL AUTO_INCREMENT,
                name varchar(255) NOT NULL,
                code varchar(255) NOT NULL,
                date_created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                date_updated timestamp,
                PRIMARY KEY (id)
            );
        ");
        $wpdb->query("
            CREATE TABLE IF NOT EXISTS {$prefix}hendra_tariff_touring_person_type (
                id int NOT NULL AUTO_INCREMENT,
                name varchar(255) NOT NULL,
                code varchar(255) NOT NULL,
                date_created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                date_updated timestamp,
                PRIMARY KEY (id)
            );
        ");
        $wpdb->query("
            CREATE TABLE IF NOT EXISTS {$prefix}hendra_tariff_touring_pitch_type (
                id int NOT NULL AUTO_INCREMENT,
                name varchar(255) NOT NULL,
                code varchar(255) NOT NULL,
                date_created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                date_updated timestamp,
                PRIMARY KEY (id)
            );
        ");
        $wpdb->query("
            CREATE TABLE IF NOT EXISTS {$prefix}hendra_tariff_touring_calendar (
                id INT NOT NULL AUTO_INCREMENT,
                person_type_id INT DEFAULT NULL,
                pitch_type_id INT DEFAULT NULL,
                extra_id INT DEFAULT NULL,
                arrival_date datetime NOT NULL,
                duration INT NOT NULL DEFAULT 1,
                price DECIMAL(13, 2) NOT NULL,
                discount DECIMAL(13, 2) NOT NULL,
                date_created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                date_updated timestamp,
                PRIMARY KEY (id),
                FOREIGN KEY (person_type_id) REFERENCES {$prefix}hendra_tariff_touring_person_type(id) ON DELETE CASCADE,
                FOREIGN KEY (pitch_type_id) REFERENCES {$prefix}hendra_tariff_touring_pitch_type(id) ON DELETE CASCADE,
                FOREIGN KEY (extra_id) REFERENCES {$prefix}hendra_tariff_touring_extra(id) ON DELETE CASCADE
            );
        ");
    }

    private function createGlamping () {
        global $wpdb;
        $prefix = $wpdb->prefix;
        $wpdb->query("
            CREATE TABLE IF NOT EXISTS {$prefix}hendra_tariff_glamping_pod (
                id int NOT NULL AUTO_INCREMENT,
                name varchar(255) NOT NULL,
                code varchar(255) NOT NULL,
                `order` int NOT NULL DEFAULT 0,
                date_created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                date_updated timestamp,
                PRIMARY KEY (id)
            );
        ");
        $wpdb->query("
            CREATE TABLE IF NOT EXISTS {$prefix}hendra_tariff_glamping_calendar (
                id INT NOT NULL AUTO_INCREMENT,
                glamping_pod_id INT DEFAULT NULL,
                arrival_date datetime NOT NULL,
                duration INT NOT NULL DEFAULT 1,
                price DECIMAL(13, 2) NOT NULL,
                discount DECIMAL(13, 2) NOT NULL,
                date_created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                date_updated timestamp,
                PRIMARY KEY (id),
                FOREIGN KEY (glamping_pod_id) REFERENCES {$prefix}hendra_tariff_glamping_pod(id) ON DELETE CASCADE
            );
        ");
    }

    private function createDatesConfig () {
        global $wpdb;
        $prefix = $wpdb->prefix;
        $wpdb->query("
            CREATE TABLE IF NOT EXISTS {$prefix}hendra_tariff_dates_config (
                id int NOT NULL AUTO_INCREMENT,
                month int NOT NULL DEFAULT 0,
                year int NOT NULL DEFAULT 0,
                date_created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                date_updated timestamp,
                PRIMARY KEY (id)
            );
        ");
    }

    private function alterHolidayHomes () {
        global $wpdb;
        $prefix = $wpdb->prefix;
        $wpdb->query("
            ALTER TABLE 
                `{$prefix}hendra_tariff_static`
            ADD
                `order` int NOT NULL DEFAULT 0
        ");
        $wpdb->query("
            ALTER TABLE 
                `{$prefix}hendra_tariff_calendar_statics`
            ADD
                `fully_booked` int NOT NULL DEFAULT 0
        ");
    }

}