<div class="hendra-tariff-page" id="hendra-tariff-glamping-pods-page">
    <div class="hendra-tariff-page-content">
        <div class="hendra-tariff-title-bar">
            <div class="hendra-tariff-title-bar-text">
                <h1 class="hendra-tariff-title-bar-title">Glamping Pods</h1>
                <p class="hendra-tariff-title-bar-desc">Manage your glamping pods here.</p>
            </div>
            <div class="hendra-tariff-title-bar-actions">
                <button class="hendra-tariff-action-btn create pod">Add Pod</button>
            </div>
        </div>
        <div class="hendra-tariff-glamping-pods-table"></div>
    </div>
</div>