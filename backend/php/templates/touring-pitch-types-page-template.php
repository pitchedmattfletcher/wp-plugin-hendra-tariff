<div class="hendra-tariff-page" id="touring-person-types-page">
    <div class="hendra-tariff-page-content">
        <div class="hendra-tariff-title-bar">
            <div class="hendra-tariff-title-bar-text">
                <h1 class="hendra-tariff-title-bar-title">Pitch Types (Touring)</h1>
                <p class="hendra-tariff-title-bar-desc">Manage your pitch types here.</p>
            </div>
            <div class="hendra-tariff-title-bar-actions">
                <button class="hendra-tariff-action-btn create pitch-type">Add Pitch</button>
            </div>
        </div>
        <div class="hendra-tariff-pitch-types-table"></div>
    </div>
</div>