<div class="hendra-tariff-page" id="hendra-tariff-import-page">
    <div class="hendra-tariff-page-content">
        <div class="hendra-tariff-title-bar">
            <div class="hendra-tariff-title-bar-text">
                <h1 class="hendra-tariff-title-bar-title">Touring Tariff Importer</h1>
                <h2 class="hendra-tariff-title-bar-current-tariff"></h2>
            </div>
            <div class="hendra-tariff-title-bar-actions">

            </div>
        </div>
        <div class="tariff-import-actions">
            <div class="tariff-import-action">
                <p>Import a csv file to generate a new touring tariff. This will override your current tariff.</p>
                <button class="hendra-tariff-action-btn import">Import Tariff</button>
            </div>
            <div class="tariff-import-action">
                <p>Export your current touring tariff to a csv file. You can make changes then re-import the file.</p>
                <button class="hendra-tariff-action-btn export">Export Tariff</button>
            </div>
            <div class="tariff-import-action">
                <p>Download a starter .csv file for your touring tariff (Highly recommended).</p>
                <button class="hendra-tariff-action-btn download">Download Template</button>
            </div>
            <div class="tariff-import-action">
                <p>Clear and delete your current touring tariff.</p>
                <button class="hendra-tariff-action-btn clear">Delete Tariff</button>
            </div>
        </div>
    </div>
</div>