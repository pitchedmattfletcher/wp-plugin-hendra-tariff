<div class="hendra-tariff-page" id="hendra-tariff-ranges-page">
    <div class="hendra-tariff-page-content">
        <div class="hendra-tariff-title-bar">
            <div class="hendra-tariff-title-bar-text">
                <h1 class="hendra-tariff-title-bar-title">Ranges</h1>
                <p class="hendra-tariff-title-bar-desc">Manage your holiday home ranges here. These will be assigned to static caravans.</p>
            </div>
            <div class="hendra-tariff-title-bar-actions">
                <button class="hendra-tariff-action-btn create range">Add Range</button>
            </div>
        </div>
        <div class="hendra-tariff-ranges-table"></div>
    </div>
</div>