<div class="hendra-tariff-page" id="touring-extras-page">
    <div class="hendra-tariff-page-content">
        <div class="hendra-tariff-title-bar">
            <div class="hendra-tariff-title-bar-text">
                <h1 class="hendra-tariff-title-bar-title">Extras (Touring)</h1>
                <p class="hendra-tariff-title-bar-desc">Manage your extras types here.</p>
            </div>
            <div class="hendra-tariff-title-bar-actions">
                <button class="hendra-tariff-action-btn create extra">Add Extra</button>
            </div>
        </div>
        <div class="hendra-tariff-extras-table"></div>
    </div>
</div>