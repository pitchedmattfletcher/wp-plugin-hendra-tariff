<div class="hendra-tariff-page" id="hendra-tariff-ranges-page">
    <div class="hendra-tariff-page-content">
        <div class="hendra-tariff-title-bar">
            <div class="hendra-tariff-title-bar-text">
                <h1 class="hendra-tariff-title-bar-title">Static Caravans</h1>
                <p class="hendra-tariff-title-bar-desc">Manage your static caravans here. Each caravan should be assigned to a range. Make sure to add at least one model to a caravan after creating.</p>
            </div>
            <div class="hendra-tariff-title-bar-actions">
                <button class="hendra-tariff-action-btn create caravan">Add Caravan</button>
            </div>
        </div>
        <div class="hendra-tariff-statics-table"></div>
    </div>
</div>