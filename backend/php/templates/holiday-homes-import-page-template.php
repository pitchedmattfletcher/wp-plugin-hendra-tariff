<div class="hendra-tariff-page" id="hendra-tariff-import-page">
    <div class="hendra-tariff-page-content">
        <div class="hendra-tariff-title-bar">
            <div class="hendra-tariff-title-bar-text">
                <h1 class="hendra-tariff-title-bar-title">Holiday Homes Tariff Importer</h1>
                <h2 class="hendra-tariff-title-bar-current-tariff"></h2>
            </div>
            <div class="hendra-tariff-title-bar-actions">

            </div>
        </div>
        <div class="tariff-import-actions">
            <div class="tariff-import-action">
                <p>Import a csv file to generate a new holiday homes tariff. This will override your current tariff.</p>
                <button class="hendra-tariff-action-btn import">Import Tariff</button>
            </div>
            <div class="tariff-import-action">
                <p>Export your current holiday homes tariff to a csv file. You can make changes then re-import the file.</p>
                <button class="hendra-tariff-action-btn export">Export Tariff</button>
            </div>
            <div class="tariff-import-action">
                <p>Download a starter .csv file for your holiday homes tariff (Highly recommended).</p>
                <!--<a href="<?php echo plugin_dir_url(__DIR__) . '../csv/holiday-homes-import-example.csv' ?>" download="holiday-homes-import-example.csv" class="hendra-tariff-action-btn download">Download Template</a>-->
                <button class="hendra-tariff-action-btn download">Download Template</button>
            </div>
            <div class="tariff-import-action">
                <p>Clear and delete your current holiday homes tariff.</p>
                <button class="hendra-tariff-action-btn clear">Delete Tariff</button>
            </div>
        </div>
    </div>
</div>