<div class="hendra-tariff-page" id="dates-config-page">
    <div class="hendra-tariff-page-content">
        <div class="hendra-tariff-title-bar">
            <div class="hendra-tariff-title-bar-text">
                <h1 class="hendra-tariff-title-bar-title">Dates Config</h1>
                <p class="hendra-tariff-title-bar-desc">The date selection box will only show the dates you've added within this page.</p>
            </div>
            <div class="hendra-tariff-title-bar-actions">
                <button class="hendra-tariff-action-btn create date-config">Add Date</button>
            </div>
        </div>
        <div class="hendra-tariff-dates-config-table"></div>
    </div>
</div>