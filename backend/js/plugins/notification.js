(function ($) {

    // Append container
    $('body').append("<div id='lucid-notifications-container'>" +
        "<ul id='lucid-notifications-list'></ul>" +
    "</div>");

    // Set the plugin name
    var PLUGIN_NAME = "notification";

    // Set the plugin defaults
    var PLUGIN_DEFAULTS = {
        'buttonLabel': 'OK',
        'time': 3200,
        'onClose': null,
    };

    // Notification template
    var TEMPLATE = "<li class='lucid-notification'>" +
        "<div class='lucid-notification-message'></div>" +
        "<div class='lucid-notification-progress'></div>" +
        "<button class='lucid-notification-button'>OK</button>" +
    "</li>";

    // Expose plugin to jquery object
    $.extend({
        [PLUGIN_NAME]: function(message, options) {
            return new Plugin(message, options);
        }
    });
    
    function Plugin (message, options) {
        this.settings = $.extend({}, PLUGIN_DEFAULTS, options);
        this.settings.message = message;
        this.element = $(TEMPLATE);
        this._closeTimeout = null;
        $('#lucid-notifications-container').append(this.element);
        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {

        init: function() {
            var that = this;
            this.message(this.settings.message);
            this.buttonLabel(this.settings.buttonLabel);
            this.time(this.settings.time);
            this.onClose(this.settings.onClose);

            // Set timeout close
            this._closeTimeout = setTimeout(function () {
                that.close(false);
            }, this.settings.time);

            // Click okay button
            this.element.find('.lucid-notification-button').on('click', function () {
                that.close(true);
            })

        },

        message: function(val) {
            if (typeof val === 'undefined') {
                return this.settings.message;
            } else {
                this.settings.message = val;
                this.element.find('.lucid-notification-message').text(val);
            }
        },

        buttonLabel: function (val) {
            if (typeof val === 'undefined') {
                return this.settings.buttonLabel;
            } else {
                this.settings.buttonLabel = val;
                this.element.find('.lucid-notification-button').text(val);
            }
        },

        time: function(val) {
            if (typeof val === 'undefined') {
                return this.settings.time;
            } else {
                this.settings.time = val;
            }
        },

        onClose: function(val) {
            if (typeof val === 'undefined') {
                return this.settings.onClose;
            } else {
                this.settings.onClose = val;
            }
        },

        close: function (buttonClicked) {
            var that = this;
            if (this._closeTimeout !== null) 
                clearTimeout(this._closeTimeout);
            this.element.css('animation-name', 'hideLucidNotification');
            setTimeout(function (){
                that.element.remove();
                if (typeof that.settings.onClose === 'function') {
                    that.settings.onClose.call(buttonClicked ? true : false);
                }
            }, 200);
        }
    });

})(jQuery);