(function ($) {

    // Set the plugin name
    var PLUGIN_NAME = "formModal";

    // HTML Template
    var TEMPLATE = '<div class="form-modal">' +
        '<div class="form-modal-backdrop"></div>' +
        '<div class="form-modal-inner">' +
            '<div class="form-modal-title-bar">' + 
                '<div class="form-modal-title-bar-title">Modal</div>' +
            '</div>' +
            '<div class="form-modal-content">' +
                '<div class="form-modal-loader"></div>' +
                '<div class="form-modal-error-message">Error with input</div>' +
                '<form onkeydown="return event.keyCode != 13" class="form-modal-form"></form>' +
            '</div>' +
            '<div class="form-modal-footer">' + 
                '<button class="form-modal-footer-submit-button" type="submit">Save</button>' +
                '<button class="form-modal-footer-cancel-button" type="button">Cancel</button>' +
            '</div>' +
        '</div>' +
    '</div>';
    
    // Set the plugin defaults
    var PLUGIN_DEFAULTS = {
        'title': 'Form Modal',
        'formData': {},
        'formTemplate': null,
        'open': false,
        'disabled': false,
        'loading': false,
        'errorMessage': false,
        'submitButton': true,
        'submitButtonLabel': 'Submit',
        'cancelButton': true,
        'cancelButtonLabel': 'Cancel',
        'onSubmit': null,
    };

    // Expose plugin to jquery object
    $.extend({
        [PLUGIN_NAME]: function(options) {
            return new Plugin(options);
        }
    });
    
    function Plugin (options) {
        this.settings = $.extend({}, PLUGIN_DEFAULTS, options);
        this.element = $(TEMPLATE);
        $('body').append(this.element);
        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {

        init: function() {
            var that = this;
            this.title(this.settings.title);
            this.formTemplate(this.settings.formTemplate);
            this.formData(this.settings.formData);
            this.submitButton(this.settings.submitButton);
            this.submitButtonLabel(this.settings.submitButtonLabel);
            this.cancelButton(this.settings.cancelButton);
            this.cancelButtonLabel(this.settings.cancelButtonLabel);
            this.open(this.settings.open);
            this.disabled(this.settings.disabled);
            this.loading(this.settings.loading);
            this.errorMessage(this.settings.errorMessage);

            this.element.find('.form-modal-footer-cancel-button').on('click', function () {
                if (!that.settings.disabled && !that.settings.loading) {
                    that.open(false);
                }
            });

            this.element.find('.form-modal-footer-submit-button').on('click', function () {
                if (!that.settings.disabled && !that.settings.loading) {
                    that.submit();
                }
            });

            this.element.find('.form-modal-form').on('submit', function (e) {
                e.preventDefault();
                if (!that.settings.disabled && !that.settings.loading) {
                    that.submit();
                }
                return false;
            })
        },

        title: function (val) {
            if (typeof val === 'undefined') {
                return this.settings.title;
            } else {
                this.settings.title = val;
                this.element.find('.form-modal-title-bar-title').text(val);
            }
        },

        open: function (val) {
            if (typeof val === 'undefined') {
                return this.settings.open;
            } else {
                this.settings.open = val;
                if (val == true) {
                    this.element.stop().fadeIn(200);
                    this.element.find('input:not([readonly],[disabled]):first').focus();
                } else {
                    this.element.stop().fadeOut(200);
                }
            }
        },

        disabled: function (val) {
            if (typeof val === 'undefined') {
                return this.settings.disabled;
            } else {
                this.settings.disabled = val;
                //this.element.css('pointer-events', val ? 'none' : 'initial');
                this.element.find("input, select, button").each(function () {
                    $(this).prop('disabled', val);
                });
            }
        },

        loading: function (val) {
            if (typeof val === 'undefined') {
                return this.settings.loading;
            } else {
                this.settings.loading = val;
                if (val) {
                    this.element.find('.form-modal-loader').stop().fadeIn(200);
                } else {
                    this.element.find('.form-modal-loader').stop().fadeOut(200);
                }
            }
        },

        submitButton: function (val) {
            if (typeof val === 'undefined') {
                return this.settings.submitButton;
            } else {
                this.settings.submitButton = val;
                if (val === true) {
                    this.element.find('.form-modal-footer-submit-button').show();
                } else {
                    this.element.find('.form-modal-footer-submit-button').hide();
                }
            }
        },

        submitButtonLabel: function (val) {
            if (typeof val === 'undefined') {
                return this.settings.submitButtonLabel;
            } else {
                this.settings.submitButtonLabel = val;
                this.element.find('.form-modal-footer-submit-button').text(val);
            }
        },

        cancelButton: function (val) {
            if (typeof val === 'undefined') {
                return this.settings.cancelButton;
            } else {
                this.settings.cancelButton = val;
                if (val === true) {
                    this.element.find('.form-modal-footer-cancel-button').show();
                } else {
                    this.element.find('.form-modal-footer-cancel-button').hide();
                }
            }
        },

        cancelButtonLabel: function (val) {
            if (typeof val === 'undefined') {
                return this.settings.cancelButtonLabel;
            } else {
                this.settings.cancelButtonLabel = val;
                this.element.find('.form-modal-footer-cancel-button').text(val);
            }
        },

        formData: function(val) {
            if (typeof val === 'undefined') {
                var formData = {};
                var data = this.element.find('.form-modal-form').serializeArray() || [];
                for (var i = 0; i < data.length; i++) {
                    formData[data[i].name] = data[i].value;
                }
                return formData;
            } else {
                for (var property in val) {
                    var el = this.element.find(".form-modal-form input[name='" + property + "']");
                    if (el.length === 0) {
                        el = this.element.find(".form-modal-form select[name='" + property + "']");
                    }
                    if (el.length > 0) {
                        el.val(val[property]);
                    }
                }
            }
        },

        formTemplate: function (val) {
            if (typeof val === 'undefined') {
                return this.settings.formTemplate;
            } else {
                this.settings.formTemplate = val;
                if (typeof this.settings.formTemplate === 'string') {
                    this.element.find('.form-modal-form').empty().append(this.settings.formTemplate);
                }
            }
        },

        errorMessage: function (val) {
            if (typeof val === 'undefined') {
                return this.settings.errorMessage;
            } else {
                if (val === false) {
                    this.element.find('.form-modal-error-message').stop().fadeOut(200);
                } else {
                    this.element.find('.form-modal-error-message').stop().text(val).fadeIn(200);
                }
            }
        },

        onSubmit: function (val) {
            if (typeof val === 'undefined') {
                return this.settings.onSubmit;
            } else {
                this.settings.onSubmit = val;
            }
        },

        submit: function () {
            if (!this.settings.disabled && !this.settings.loading && typeof this.settings.onSubmit === 'function') {
                this.settings.onSubmit.call(this, this, this.formData());
            }
        },

        destroy: function () {
        }

    });


})(jQuery);