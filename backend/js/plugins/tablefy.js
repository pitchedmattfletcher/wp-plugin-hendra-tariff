(function ($) {

    // Set the plugin name
    var PLUGIN_NAME = "tablefy";

    // HTML Template
    var TEMPLATE = '<table class="tablefy-table">' +
        '<thead class="tablefy-thead">' +
            '<tr class="tablefy-tr"></tr>' + 
        '</thead>' +
        '<tbody class="tablefy-tbody"></tbody>' +
    '</table>';
    
    // Set the plugin defaults
    var PLUGIN_DEFAULTS = {
        'columnModel': [],
        'rowModel': [],
        'clickableCellClass': false, // Implement this
    };

    // Default for each column
    var COLUMN_DEFAULTS = {
        'label': "-",
        'fieldName': "-",
        'width': null,
        'align': "left",
        'hidden': false,
        'action': false
    }

    // Plugin constructor
    function Plugin (element, options) {
        this.element = $(element);
        this.settings = $.extend({}, PLUGIN_DEFAULTS, options);
        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {

        init: function() {
            this.element.addClass("tablefy");
            this.element.append(TEMPLATE);       
            this.columnModel(this.settings.columnModel);
            this.rowModel(this.settings.rowModel);
            this.bindEvents();
        },

        bindEvents: function () {
            var that = this;

            // Clicking individual cells
            this.element.find("tbody").on('click', function (e) {
                var rowIndex = e.target.parentNode.rowIndex;
                var columnIndex = e.target.cellIndex;
                if (rowIndex === 0)
                    return;
                var col = that.settings.columnModel[columnIndex];
                if (col && typeof col.action === 'function') {
                    col.action.call(this, that.settings.rowModel[rowIndex - 1]);
                }
            });

        },

        columnModel: function (val) {
            if (typeof val === 'undefined') {
                return this.settings.columnModel;
            } else {
                this.settings.columnModel = val;
                
                // Reference elements
                var tableHeadEl = this.element.find(".tablefy-table thead.tablefy-thead tr.tablefy-tr");

                // Empty inputs and table head
                tableHeadEl.empty();

                // Create column headers
                for (var i = 0; i < this.settings.columnModel.length; i++) {
                    this.settings.columnModel[i] = $.extend({}, COLUMN_DEFAULTS, this.settings.columnModel[i]);
                    var node = $("<th class='tablefy-th'>");
                    var cssText = "text-align:" + this.settings.columnModel[i].align + "!important;";
                    node.text(this.settings.columnModel[i].label);
                    if (this.settings.columnModel[i].width !== null) {
                        cssText += "width:" + this.settings.columnModel[i].width + "!important;" + "max-width:" + this.settings.columnModel[i].width + "!important;" + "min-width:" + this.settings.columnModel[i].width + "!important;";
                    }
                    if (this.settings.columnModel[i].hidden) {
                        cssText += "display:none";
                    }
                    node.attr('style', cssText);
                    tableHeadEl.append(node);
                }
            }
        },

        rowModel: function (val) {
            if (typeof val === 'undefined') {
                return this.settings.rowModel;
            } else {
                this.settings.rowModel = val;

                // Reference table body
                var tableBodyEl = this.element.find("tbody");

                // Empty body
                tableBodyEl.empty();

                for (var i = 0; i < this.settings.rowModel.length; i++) {
                    var tr = $("<tr class='tablefy-tr'>");
                    for (var j = 0; j < this.settings.columnModel.length; j++) {
                        var td = $("<td class='tablefy-td'>");
                        var cssText = "text-align:" + this.settings.columnModel[j].align + ";";
                        var fieldValue = this.settings.rowModel[i][this.settings.columnModel[j].field_name] ? this.settings.rowModel[i][this.settings.columnModel[j].field_name] : '';
                        td.html(fieldValue);
                        if (this.settings.columnModel[j].hidden) 
                            cssText += "display:none;";
                        td.attr('style',cssText);
                        if (this.settings.columnModel[j].action) {
                            td.addClass("tablefy-td-action");
                            if (this.settings.clickableCellClass) {
                                td.addClass(this.settings.clickableCellClass);
                            }
                        }
                        tr.append(td);
                    }
                    tableBodyEl.append(tr);
                }
            }
        },

        destroy: function () {
            this.element.find('tbody').off('click');
        }

    });

    // You don't need to change anything below:
    $.fn[PLUGIN_NAME] = function ( options ) {
        var args = arguments;
        if (options === undefined || typeof options === 'object') {
            return this.each(function () {
                if (!$.data(this, 'plugin_' + PLUGIN_NAME)) {
                    $.data(this, 'plugin_' + PLUGIN_NAME, new Plugin( this, options ));
                }
            });
        } else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {
            var returns;
            this.each(function () {
                var instance = $.data(this, 'plugin_' + PLUGIN_NAME);
                if (instance instanceof Plugin && typeof instance[options] === 'function') {
                    returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1 ) );
                }
                if (options === 'destroy') {
                    $.data(this, 'plugin_' + PLUGIN_NAME, null);
                }
            });
            return returns !== undefined ? returns : this;
        }
    };


})(jQuery);