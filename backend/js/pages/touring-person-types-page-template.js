(function ($) {

    // Init DAOs
    var personTypesDAO = new HendraTariff.DAOs.PersonTypesDAO();

    // Reference some elements
    var createPersonTypeBtn = $('.hendra-tariff-action-btn.create.person-type');
    var personTypesTable = $('.hendra-tariff-person-types-table');

    // Init tablefy on ranges
    personTypesTable.tablefy({
        'columnModel': [
            { label: "ID", field_name: "id", width: '120px' },
            { label: "Name", field_name: "name" },
            { label: "Shortcode", field_name: "code" },
            { label: "Update", field_name: "update", align: "center", width: "120px", action: openUpdatePersonTypeModal },
            { label: "Delete", field_name: "delete", align: "center", width: "120px", action: openDeletePersonTypeModal }
        ]
    });
    
    // Init the create person type modal.
    var createPersonTypeModal = $.formModal({

        // The modal title
        'title': 'Create Person Type',

        // Set the submit button text
        'submitButtonLabel': 'Create',

        // The form template
        'formTemplate':
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Name</div>' +
                '<input type="text" name="name" value="" placeholder="E.g. Adult (Aged 15+)"  />' +
            '</div>' + 
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Shortcode (This will be used within your spreadsheet)</div>' +
                '<input type="text" name="code" value="" placeholder="E.g PTADULT"  />' +
            '</div>',

        // Handle the on submit event
        'onSubmit': function (instance, data) {

            // Disable form, and show loading icon
            instance.disabled(true);
            instance.loading(true);

            // Create the new range
            personTypesDAO.createPersonType({ name: data.name, code: data.code, }, function (response) {
 
                // Form is no longer loading and disabled
                instance.disabled(false);
                instance.loading(false);
                
                // If there was an error
                if (!response.success) {
                    instance.errorMessage(response.message);
                } else {
                    instance.errorMessage(false);
                    instance.open(false);
                    $.notification(response.message);
                    renderPersonTypes();
                }

            });
        }
    });

    // Init the edit person type modal
    var editPersonTypeModal = $.formModal({

        // The modal title
        'title': 'Update Person Type',

        // Set the submit button text
        'submitButtonLabel': 'Save',

        // The form template
        'formTemplate':
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Person Type ID</div>' +
                '<input type="text" name="id" value="" readonly/>' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Name</div>' +
                '<input type="text" name="name" value="" placeholder="E.g. Adult (Aged 15+)"  />' +
            '</div>' + 
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Shortcode (This will be used within your spreadsheet)</div>' +
                '<input type="text" name="code" value="" placeholder="E.g PTADULT"  />' +
            '</div>',

        // Handle the on submit event
        'onSubmit': function (instance, data) {

            // Disable form, and show loading icon
            instance.disabled(true);
            instance.loading(true);

            // Create the new range
            personTypesDAO.updatePersonType({ name: data.name, id: data.id, code: data.code }, function (response) {
    
                // Form is no longer loading and disabled
                instance.disabled(false);
                instance.loading(false);

                // If there was an error
                if (!response.success) {
                    instance.errorMessage(response.message);
                } else {
                    instance.errorMessage(false);
                    instance.open(false);
                    $.notification(response.message);
                    renderPersonTypes();
                }

            });
        }
    });

    // Init the delete range modal
    var deletePersonType = $.formModal({

        // The modal title
        'title': 'Delete Person Type',

        // Set the submit button text
        'submitButtonLabel': 'Delete',

        // The form template
        'formTemplate':
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Person Type ID</div>' +
                '<input type="text" name="id" value="" readonly/>' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Name</div>' +
                '<input type="text" name="name" value="" placeholder="E.g. Adult (Aged 15+)"  readonly/>' +
            '</div>' + 
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Shortcode</div>' +
                '<input type="text" name="code" value="" placeholder="E.g PTADULT"  readonly/>' +
            '</div>',

        // Handle the on submit event
        'onSubmit': function (instance, data) {

            // Disable form, and show loading icon
            instance.disabled(true);
            instance.loading(true);

            // Create the new range
            personTypesDAO.deletePersonType(data.id, function (response) {
    
                // Form is no longer loading and disabled
                instance.disabled(false);
                instance.loading(false);

                // If there was an error
                if (!response.success) {
                    instance.errorMessage(response.message);
                } else {
                    instance.errorMessage(false);
                    instance.open(false);
                    $.notification(response.message);
                    renderPersonTypes();
                }

            });
        }
    });

    // Add click event to the create range button
    createPersonTypeBtn.on('click', openCreatePersonTypeModal);

    function openCreatePersonTypeModal () {
        createPersonTypeModal.errorMessage(false);
        createPersonTypeModal.formData({ name: '', code: '' });
        createPersonTypeModal.open(true);
    }

    function openUpdatePersonTypeModal (personType) {
        if (personType.id === '-')
            return;
        editPersonTypeModal.formData({
            id: personType.id,
            name: personType.name,
            code: personType.code,
        });
        editPersonTypeModal.errorMessage(false);
        editPersonTypeModal.open(true);
    }

    function openDeletePersonTypeModal (personType) {
        if (personType.id === '-')
            return;
        deletePersonType.formData({
            id: personType.id,
            code: personType.code,
            name: personType.name,
        });
        deletePersonType.open(true);
    }


    function renderPersonTypes () {
        personTypesTable.tablefy('rowModel', [{ id: '-', name: '-', code: '-', update: '-', delete: '-' }]);
        personTypesDAO.getPersonTypes(function (response) {
            console.log(response.data);
            var rowModel = [];
            for (var i = 0; i < response.data.length; i++) {
                rowModel.push({
                    id: response.data[i].id,
                    name: response.data[i].name,
                    code: response.data[i].code,
                    update: '<span class="tablefy-icon update"></span>',
                    delete: '<span class="tablefy-icon delete"></span>'
                })
            }
            rowModel = rowModel.length > 0 ? rowModel : [{ id: '-', name: '-', code: '-', update: '-', delete: '-' }]
            personTypesTable.tablefy('rowModel', rowModel);
        });
    }

    renderPersonTypes();


})(jQuery);
