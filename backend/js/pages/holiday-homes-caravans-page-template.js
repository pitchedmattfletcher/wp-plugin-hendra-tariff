(function ($) {

    // Init DAOs
    var rangesDAO = new HendraTariff.DAOs.RangesDAO();
    var staticsDAO = new HendraTariff.DAOs.StaticsDAO();
    var modelsDAO = new HendraTariff.DAOs.ModelsDAO();

    // Reference some elements
    var createStaticBtn = $('.hendra-tariff-action-btn.create.caravan');
    var staticsTable = $('.hendra-tariff-statics-table');

    // Init tablefy on statics
    staticsTable.tablefy({
        'columnModel': [
            { label: "ID", field_name: "id", width: '120px' },
            { label: "Name", field_name: "name" },
            { label: "Range", field_name: "range_name" },
            { label: "Range ID", field_name: "range_id", hidden: true },
            { label: "Order", field_name: "order" },
            { label: "Flash", field_name: "flash" },
            { label: "Models", field_name: "models", align: "center", width: "120px", action: openEditStaticModelsModal },
            { label: "Update", field_name: "update", align: "center", width: "120px", action: openUpdateStaticModal },
            { label: "Delete", field_name: "delete", align: "center", width: "120px", action: openDeleteStaticModal }
        ]
    });

    // Init the create range modal.
    var createStaticModal = $.formModal({
        'title': 'Create Static',
        'submitButtonLabel': 'Create',
        'formTemplate':
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Name</div>' +
                '<input type="text" name="name" value="" placeholder="E.g. Whipsiderry" />' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Flash (optional)</div>' +
                '<input type="text" name="flash" value="" placeholder="E.g. New For 2020!" />' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Range</div>' +
                '<select name="range_id"></select>' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Order</div>' +
                '<input type="number" name="order" value="0" />' +
            '</div>',
        'onSubmit': function (instance, data) {

            // Disable form, and show loading icon
            instance.disabled(true);
            instance.loading(true);

            // Create the new range
            staticsDAO.createStatic({ name: data.name, flash: data.flash, range_id: data.range_id, order: data.order }, function (response) {
 
                // Form is no longer loading and disabled
                instance.disabled(false);
                instance.loading(false);
                
                // If there was an error
                if (!response.success) {
                    instance.errorMessage(response.message);
                } else {
                    instance.errorMessage(false);
                    instance.open(false);
                    $.notification(response.message);
                    renderStatics();
                }

            });
        }
    });

    // Init the create range modal.
    var editStaticModal = $.formModal({
        'title': 'Edit Static',
        'submitButtonLabel': 'Save',
        'formTemplate':
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">ID</div>' +
                '<input type="text" name="id" value="" readonly />' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Name</div>' +
                '<input type="text" name="name" value="" placeholder="E.g. Whipsiderry" />' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Flash (optional)</div>' +
                '<input type="text" name="flash" value="" placeholder="E.g. New For 2020!" />' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Range</div>' +
                '<select name="range_id"></select>' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Order</div>' +
                '<input type="number" name="order" value="0" />' +
            '</div>',
        'onSubmit': function (instance, data) {

            // Disable form, and show loading icon
            instance.disabled(true);
            instance.loading(true);

            // Create the new range
            staticsDAO.updateStatic({ id: data.id, name: data.name, flash: data.flash, range_id: data.range_id, order: data.order }, function (response) {
 
                // Form is no longer loading and disabled
                instance.disabled(false);
                instance.loading(false);
                
                // If there was an error
                if (!response.success) {
                    instance.errorMessage(response.message);
                } else {
                    instance.errorMessage(false);
                    instance.open(false);
                    $.notification(response.message);
                    renderStatics();
                }

            });
        }
    });

    // Init the delete static modal
    var deleteStaticModal = $.formModal({
        'title': 'Delete Static',
        'submitButtonLabel': 'Delete',
        'formTemplate':
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Static ID</div>' +
                '<input type="text" name="id" value="" readonly />' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Static Name</div>' +
                '<input type="text" name="name" value="" readonly />' +
            '</div>',
        'onSubmit': function (instance, data) {

            // Disable form, and show loading icon
            instance.disabled(true);
            instance.loading(true);

            // Create the new range
            staticsDAO.deleteStatic(data.id, function (response) {
 
                // Form is no longer loading and disabled
                instance.disabled(false);
                instance.loading(false);

                // If there was an error
                if (!response.success) {
                    instance.errorMessage(response.message);
                } else {
                    instance.errorMessage(false);
                    instance.open(false);
                    $.notification(response.message);
                    renderStatics();
                }

            });
        }
    });

    // Init the delete static modal
    var editStaticModelsModal = $.formModal({

        // The modal title
        'title': 'Static Models',

        // Set the submit button text
        'submitButtonLabel': 'Finish',

        // Hide the cancel button
        'cancelButton': false,

        // The form template
        'formTemplate':
            '<input type="hidden" name="static_id" value="" readonly />' +
            '<div class="form-modal-group with-btn">' +
                '<div class="form-modal-label">Add Model</div>' +
                '<input type="text" name="model_name" value="" placeholder="E.g. T3"/>' +
                '<button class="add-model form-modal-group-btn" type="button">Add</button>' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Assigned Models</div>' +
                '<ul class="form-modal-listbox models-listbox"></ul>' +
            '</div>',

        // Handle the on submit event
        'onSubmit': function (instance, data) {

            // Just simply close the form
            instance.open(false);

            // And refresh the table
            renderStatics();

        }
    });

    // Add click event to edit static models modal add button
    editStaticModelsModal.element.find('.add-model').on('click', function () {
        
        // Disable and show loading indicator.
        editStaticModelsModal.disabled(true);
        editStaticModelsModal.loading(true);

        // Get name
        var static_id = editStaticModelsModal.element.find('input[name="static_id"]').val();
        var name = editStaticModelsModal.element.find('input[name="model_name"]').val();

        // Attempt to create the model
        modelsDAO.createModel({ static_id: static_id, name: name }, function (response) {
                
            // Form is no longer loading and disabled
            editStaticModelsModal.disabled(false);
            editStaticModelsModal.loading(false);

            // If there was an error
            if (!response.success) {
                editStaticModelsModal.errorMessage(response.message);
            } else {

                // Reset model name field
                editStaticModelsModal.element.find('input[name="model_name"]').val('');
                
                // There is no error message.
                editStaticModelsModal.errorMessage(false);

                // Get static models
                modelsDAO.getModelsByStaticID(static_id, function (response) {

                    // Get listbox el
                    var listbox = editStaticModelsModal.element.find('.models-listbox');
                    
                    // Empty listbox
                    listbox.empty();

                    // Append models
                    for (var i = 0; i < response.data.length; i++) {
                        listbox.append('<li class="form-modal-listbox-item" data-id="' + response.data[i].id + '">' + response.data[i].name + '<div class="form-modal-listbox-item-remove-btn"></div></li>');
                    }

                    // Modal is no longer disabled and loading.
                    editStaticModelsModal.loading(false);
                    editStaticModelsModal.disabled(false);

                });

            }

        })

        
    });

    // Add click event to edit static models modal to remove a model
    editStaticModelsModal.element.on('click', '.form-modal-listbox-item-remove-btn', function () {
                            
        // Modal is no longer disabled and loading.
        editStaticModelsModal.loading(true);
        editStaticModelsModal.disabled(true);

        // Get model id
        var model_id = parseInt($(this).parent().attr('data-id'));
        var static_id = editStaticModelsModal.element.find('input[name="static_id"]').val();

        // Make request to delete model
        modelsDAO.deleteModel(model_id, function (response) {

            // If there was an error
            if (!response.success) {
                editStaticModelsModal.errorMessage(response.message);
                editStaticModelsModal.disabled(false);
                editStaticModelsModal.loading(false);
            } else {

                // Everything was okay, so rebuild the listbox
                modelsDAO.getModelsByStaticID(static_id, function (response) {

                    // Get listbox el
                    var listbox = editStaticModelsModal.element.find('.models-listbox');
                    
                    // Empty listbox
                    listbox.empty();

                    // Append models
                    for (var i = 0; i < response.data.length; i++) {
                        listbox.append('<li class="form-modal-listbox-item" data-id="' + response.data[i].id + '">' + response.data[i].name + '<div class="form-modal-listbox-item-remove-btn"></div></li>');
                    }

                    // Modal is no longer disabled and loading.
                    editStaticModelsModal.loading(false);
                    editStaticModelsModal.disabled(false);

                });
            }
        });
    });

    // Add click event to the create static button.
    createStaticBtn.on('click', openCreateStaticModal);

    function openCreateStaticModal() {

        // Disable and show loading indicator
        createStaticModal.loading(true);
        createStaticModal.disabled(true);
        createStaticModal.errorMessage(false);

        // Clear select range select
        createStaticModal.element.find('select[name="range_id"]').empty();

        // Reset modal form data
        createStaticModal.formData({ 
            'name': '',
            'range_id': '',
            'flash': '',
        });

        // Open modal
        createStaticModal.open(true);

        // Query ranges
        rangesDAO.getRanges(function (result) {

            // Populate select
            var select = createStaticModal.element.find('select[name="range_id"]');
            for (var i = 0; i < result.data.length; i++) {
                select.append('<option value="' + result.data[i].id + '">' + result.data[i].name + '</option>');
            }

            // Enable modal
            createStaticModal.loading(false);
            createStaticModal.disabled(false);

        });
    }

    function openUpdateStaticModal (static) {
        if (static.id === '-') return;

        // Disable and show loading indicator
        editStaticModal.loading(true);
        editStaticModal.disabled(true);

        // Clear select range select
        editStaticModal.element.find('select[name="range_id"]').empty();

        // Reset modal form data
        editStaticModal.formData({ 
            'id': static.id,
            'name': static.name,
            'flash': static.flash === '-' ? '' : static.flash,
            'range_id': '',
            'order': static.order,
        });

        // Open modal
        editStaticModal.open(true);

        // Query ranges
        rangesDAO.getRanges(function (result) {

            // Populate select
            var select = editStaticModal.element.find('select[name="range_id"]');
            for (var i = 0; i < result.data.length; i++) {
                if (parseInt(result.data[i].id) === parseInt(static.range_id)) {
                    select.append('<option value="' + result.data[i].id + '" selected>' + result.data[i].name + '</option>');
                } else {
                    select.append('<option value="' + result.data[i].id + '">' + result.data[i].name + '</option>');
                }
            }

            // Enable modal
            editStaticModal.loading(false);
            editStaticModal.disabled(false);

        });
    }

    function openEditStaticModelsModal (static) {
        if (static.id !== '-') {

            // Disable and show loading indicator
            editStaticModelsModal.errorMessage(false);
            editStaticModelsModal.loading(true);
            editStaticModelsModal.disabled(true);

            // Set title
            editStaticModelsModal.title('Edit ' + static.name + ' Models');

            // Reset modal form data
            editStaticModelsModal.formData({ 
                'static_id': static.id,
                'model_name': '',
            });

            // Open modal
            editStaticModelsModal.open(true);

            // Get static models
            modelsDAO.getModelsByStaticID(static.id, function (response) {

                // Get listbox el
                var listbox = editStaticModelsModal.element.find('.models-listbox');
                
                // Empty listbox
                listbox.empty();

                // Append models
                for (var i = 0; i < response.data.length; i++) {
                    listbox.append('<li class="form-modal-listbox-item" data-id="' + response.data[i].id + '">' + response.data[i].name + '<div class="form-modal-listbox-item-remove-btn"></div></li>');
                }

                // Modal is no longer disabled and loading.
                editStaticModelsModal.loading(false);
                editStaticModelsModal.disabled(false);

            });
        }
    }

    function openDeleteStaticModal (static) {
        if (static.id !== '-') {
            deleteStaticModal.formData({
                id: static.id,
                name: static.name,
            });
            deleteStaticModal.open(true);
        }
    }

    function renderStatics () {
        staticsTable.tablefy('rowModel', [ { id: '-', name: '-', range_name:'-', flash: '-', models: '-', update: '-', delete: '-' } ]);
        staticsDAO.getStatics(function (response) {
            var rowModel = [];
            for (var i = 0; i < response.data.length; i++) {
                rowModel.push({
                    id: parseInt(response.data[i].id),
                    name: response.data[i].name,
                    flash: response.data[i].flash ? response.data[i].flash : '-',
                    order: response.data[i].order,
                    range_name: response.data[i].range_name,
                    range_id: parseInt(response.data[i].range_id),
                    models: 'Edit (' + response.data[i].amount_models + ')',
                    update: '<span class="tablefy-icon update"></span>',
                    delete: '<span class="tablefy-icon delete"></span>'
                });
            }
            rowModel = rowModel.length > 0 ? rowModel : [{ id: '-', name: '-', range_name:'-', flash: '-', models: '-', update: '-', delete: '-' }]
            staticsTable.tablefy('rowModel', rowModel);
        });
    }

    renderStatics();

})(jQuery);
