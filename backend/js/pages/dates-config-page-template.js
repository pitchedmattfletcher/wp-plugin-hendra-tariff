(function ($) {

    // Init DAOs
    var datesConfigDAO = new HendraTariff.DAOs.DatesConfigDAO();

    // Reference some elements
    var createDateConfigBtn = $('.hendra-tariff-action-btn.create.date-config');
    var datesConfigTable = $('.hendra-tariff-dates-config-table');

    // Init tablefy on ranges
    datesConfigTable.tablefy({
        'columnModel': [
            { label: "ID", field_name: "id", width: '120px' },
            { label: "Month", field_name: "month" },
            { label: "Year", field_name: "year" },
            { label: "Delete", field_name: "delete", align: "center", width: "120px", action: openDeleteDateConfigModal }
        ]
    });
    
    // Init the create person type modal.
    var createDateConfigModal = $.formModal({

        // The modal title
        'title': 'Create Date',

        // Set the submit button text
        'submitButtonLabel': 'Create',

        // The form template
        'formTemplate':
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Year</div>' +
                '<input type="number" name="year" value="" placeholder="E.g. 2022"  />' +
            '</div>' + 
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Month (1 = January, 12 = December)</div>' +
                '<input type="number" name="month" value="" min="1" max="12" placeholder="E.g 5"  />' +
            '</div>',

        // Handle the on submit event
        'onSubmit': function (instance, data) {

            // Disable form, and show loading icon
            instance.disabled(true);
            instance.loading(true);

            // Create the new range
            datesConfigDAO.createDateConfig({ year: data.year, month: data.month, }, function (response) {
 
                // Form is no longer loading and disabled
                instance.disabled(false);
                instance.loading(false);
                
                // If there was an error
                if (!response.success) {
                    instance.errorMessage(response.message);
                } else {
                    instance.errorMessage(false);
                    instance.open(false);
                    $.notification(response.message);
                    renderDateConfigs();
                }

            });
        }
    });


    // Init the delete range modal
    var deletePersonType = $.formModal({

        // The modal title
        'title': 'Delete Date',

        // Set the submit button text
        'submitButtonLabel': 'Delete',

        // The form template
        'formTemplate':
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Date ID</div>' +
                '<input type="text" name="id" value="" readonly/>' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Year</div>' +
                '<input type="text" name="year" value="" placeholder=""  readonly/>' +
            '</div>' + 
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Month</div>' +
                '<input type="text" name="month" value="" readonly/>' +
            '</div>',

        // Handle the on submit event
        'onSubmit': function (instance, data) {

            // Disable form, and show loading icon
            instance.disabled(true);
            instance.loading(true);

            // Create the new range
            datesConfigDAO.deleteDateConfig(data.id, function (response) {
    
                // Form is no longer loading and disabled
                instance.disabled(false);
                instance.loading(false);

                // If there was an error
                if (!response.success) {
                    instance.errorMessage(response.message);
                } else {
                    instance.errorMessage(false);
                    instance.open(false);
                    $.notification(response.message);
                    renderDateConfigs();
                }

            });
        }
    });

    // Add click event to the create range button
    createDateConfigBtn.on('click', openCreatePersonTypeModal);

    function openCreatePersonTypeModal () {
        createDateConfigModal.errorMessage(false);
        createDateConfigModal.formData({ name: '', code: '' });
        createDateConfigModal.open(true);
    }

    function openDeleteDateConfigModal (dateConfig) {
        if (dateConfig.id === '-')
            return;
        deletePersonType.formData({
            id: dateConfig.id,
            year: dateConfig.year,
            month: dateConfig.month,
        });
        deletePersonType.open(true);
    }

    var months = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ]

    function renderDateConfigs () {
        datesConfigTable.tablefy('rowModel', [{ id: '-', year: '-', month: '-', delete: '-' }]);
        datesConfigDAO.getDatesConfig(function (response) {
            console.log(response.data);
            var rowModel = [];
            for (var i = 0; i < response.data.length; i++) {
                rowModel.push({
                    id: response.data[i].id,
                    month: response.data[i].month + ' (' + (months[response.data[i].month - 1]) + ')',
                    year: response.data[i].year,
                    delete: '<span class="tablefy-icon delete"></span>'
                })
            }
            rowModel = rowModel.length > 0 ? rowModel : [{ id: '-', month: '-', year: '-', delete: '-' }]
            datesConfigTable.tablefy('rowModel', rowModel);
        });
    }

    renderDateConfigs();


})(jQuery);
