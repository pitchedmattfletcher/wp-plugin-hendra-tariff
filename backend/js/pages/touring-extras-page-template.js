(function ($) {

    // Init DAOs
    var extrasDAO = new HendraTariff.DAOs.ExtrasDAO();

    // Reference some elements
    var createBtn = $('.hendra-tariff-action-btn.create.extra');
    var table = $('.hendra-tariff-extras-table');

    // Init tablefy 
    table.tablefy({
        'columnModel': [
            { label: "ID", field_name: "id", width: '120px' },
            { label: "Name", field_name: "name" },
            { label: "Shortcode", field_name: "code" },
            { label: "Update", field_name: "update", align: "center", width: "120px", action: openUpdateModal },
            { label: "Delete", field_name: "delete", align: "center", width: "120px", action: openDeleteModal }
        ]
    });
    
    // Init the create modal.
    var createModal = $.formModal({

        // The modal title
        'title': 'Create Extra',

        // Set the submit button text
        'submitButtonLabel': 'Create',

        // The form template
        'formTemplate':
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Name</div>' +
                '<input type="text" name="name" value="" placeholder="E.g. Dogs Limited Fields"  />' +
            '</div>' + 
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Shortcode (This will be used within your spreadsheet)</div>' +
                '<input type="text" name="code" value="" placeholder="E.g DOGS"  />' +
            '</div>',

        // Handle the on submit event
        'onSubmit': function (instance, data) {

            // Disable form, and show loading icon
            instance.disabled(true);
            instance.loading(true);

            // Create the new range
            extrasDAO.createExtra({ name: data.name, code: data.code, }, function (response) {
 
                // Form is no longer loading and disabled
                instance.disabled(false);
                instance.loading(false);
                
                // If there was an error
                if (!response.success) {
                    instance.errorMessage(response.message);
                } else {
                    instance.errorMessage(false);
                    instance.open(false);
                    $.notification(response.message);
                    render();
                }

            });
        }
    });

    // Init the edit modal
    var editModal = $.formModal({

        // The modal title
        'title': 'Update Extra',

        // Set the submit button text
        'submitButtonLabel': 'Save',

        // The form template
        'formTemplate':
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Person Type ID</div>' +
                '<input type="text" name="id" value="" readonly/>' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Name</div>' +
                '<input type="text" name="name" value="" placeholder="E.g. Dogs Limited Fields"  />' +
            '</div>' + 
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Shortcode (This will be used within your spreadsheet)</div>' +
                '<input type="text" name="code" value="" placeholder="E.g DOGS"  />' +
            '</div>',

        // Handle the on submit event
        'onSubmit': function (instance, data) {

            // Disable form, and show loading icon
            instance.disabled(true);
            instance.loading(true);

            // Create the new range
            extrasDAO.updateExtra({ name: data.name, id: data.id, code: data.code }, function (response) {
    
                // Form is no longer loading and disabled
                instance.disabled(false);
                instance.loading(false);

                // If there was an error
                if (!response.success) {
                    instance.errorMessage(response.message);
                } else {
                    instance.errorMessage(false);
                    instance.open(false);
                    $.notification(response.message);
                    render();
                }

            });
        }
    });

    // Init the delete modal
    var deleteModal = $.formModal({

        // The modal title
        'title': 'Delete Extra',

        // Set the submit button text
        'submitButtonLabel': 'Delete',

        // The form template
        'formTemplate':
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Extra ID</div>' +
                '<input type="text" name="id" value="" readonly/>' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Name</div>' +
                '<input type="text" name="name" value="" readonly/>' +
            '</div>' + 
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Shortcode</div>' +
                '<input type="text" name="code" value="" readonly/>' +
            '</div>',

        // Handle the on submit event
        'onSubmit': function (instance, data) {

            // Disable form, and show loading icon
            instance.disabled(true);
            instance.loading(true);

            // Create the new range
            extrasDAO.deleteExtra(data.id, function (response) {
    
                // Form is no longer loading and disabled
                instance.disabled(false);
                instance.loading(false);

                // If there was an error
                if (!response.success) {
                    instance.errorMessage(response.message);
                } else {
                    instance.errorMessage(false);
                    instance.open(false);
                    $.notification(response.message);
                    render();
                }

            });
        }
    });

    // Add click event to the create button
    createBtn.on('click', openCreateModal);

    // Open the create modal
    function openCreateModal () {
        createModal.errorMessage(false);
        createModal.formData({ name: '', code: '' });
        createModal.open(true);
    }

    // Open the update modal
    function openUpdateModal (extra) {
        if (extra.id === '-')
            return;
        editModal.formData({
            id: extra.id,
            name: extra.name,
            code: extra.code,
        });
        editModal.errorMessage(false);
        editModal.open(true);
    }

    // Open the delete modal
    function openDeleteModal (extra) {
        if (extra.id === '-')
            return;
        deleteModal.formData({
            id: extra.id,
            code: extra.code,
            name: extra.name,
        });
        deleteModal.open(true);
    }

    // Render the table
    function render () {
        table.tablefy('rowModel', [{ id: '-', name: '-', code: '-', update: '-', delete: '-' }]);
        extrasDAO.getExtras(function (response) {
            var rowModel = [];
            for (var i = 0; i < response.data.length; i++) {
                rowModel.push({
                    id: response.data[i].id,
                    name: response.data[i].name,
                    code: response.data[i].code,
                    update: '<span class="tablefy-icon update"></span>',
                    delete: '<span class="tablefy-icon delete"></span>'
                })
            }
            rowModel = rowModel.length > 0 ? rowModel : [{ id: '-', name: '-', code: '-', update: '-', delete: '-' }]
            table.tablefy('rowModel', rowModel);
        });
    }

    render();

})(jQuery);
