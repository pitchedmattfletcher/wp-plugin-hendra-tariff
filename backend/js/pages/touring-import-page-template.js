(function ($) {

    // Init DAOs
    var touringTariffDAO = new HendraTariff.DAOs.TouringTariffDAO();

    // Reference elements
    var importButton = $('#hendra-tariff-import-page .hendra-tariff-action-btn.import');
    var exportButton = $('#hendra-tariff-import-page .hendra-tariff-action-btn.export');
    var clearButton = $('#hendra-tariff-import-page .hendra-tariff-action-btn.clear');
    var exampleDownloadBtn = $('#hendra-tariff-import-page .hendra-tariff-action-btn.download');
    var currentTariffInfo = $('#hendra-tariff-import-page .hendra-tariff-title-bar-current-tariff');

    // Init the importing modal
    var importModal = $.formModal({

        // The modal title
        'title': 'Import Touring Tariff',

        // Set the submit button text
        'submitButtonLabel': 'Okay',

        // The form template
        'formTemplate':
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Select CSV File</div>' +
                '<input name="csv_file" type="file"  accept=".csv"/>' +
            '</div>' + 
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Selected File Name</div>' +
                '<input name="csv_filename" type="text" readonly/>' +
            '</div>',

        // Handle the on submit event
        'onSubmit': function (instance, data) {

            // Make sure a file has been selected
            var file = selectFileInput.prop('files')[0];
			console.log(file);
            if (file) {

                // Show loading indicator
                instance.loading(true);
                instance.disabled(true);

                // hide error message
                instance.errorMessage(false);

                // Create the file reader
                var fileReader = new FileReader();

                // On file reader load
                fileReader.onload = function(e) { 

                    // Parse CSV
                    var contents = e.target.result;
                    var result = Papa.parse(contents, {
                        header: true,
                        skipEmptyLines: 'greedy',
                    });

                    // If there were errors, display the first one.
                    if (result.errors.length > 0) {
                        instance.errorMessage(result.errors[0].message);
                        instance.loading(false);
                        instance.disabled(false);
                    } else {

                        // Else we run the import ajax function.
                        touringTariffDAO.import(result.data, function (response) {

                            // Function has completed, so enable form etc.
                            instance.loading(false);
                            instance.disabled(false);

                            // If not success, show error.
                            // Else hide modal as usual.
                            if (!response.success) {
                                instance.errorMessage(response.message);
                                instance.formData({ 'csv_filename' : '', 'csv_file': '', });
                            } else {
                                instance.errorMessage(false);
                                instance.open(false);
                                $.notification(response.message);
                                renderCurrentTariffInfo();
                            }

                        });
                    }
               }

               // Read the file
               fileReader.readAsBinaryString(file);

            } else { 
                instance.errorMessage('Please select a .csv file.');
            }




            //instance.open(false);
        }
    });

    // Init the clear modal
    var clearModal = $.formModal({
        'title': 'Clear Touring Tariff',
        'submitButtonLabel': 'Clear',
        'formTemplate': '<p>Are you sure you want to clear your touring tariff? You cannot undo this.</p>',
        'onSubmit': function () {
            clearModal.loading(true);
            clearModal.disabled(true);
            touringTariffDAO.clear(function (response) {
                $.notification(response.message);
                clearModal.loading(false);
                clearModal.disabled(false);
                clearModal.open(false);
                renderCurrentTariffInfo();
            })
        }
    });

    var selectFileInput = importModal.element.find('input[name="csv_file"]');
    var selectedFileInput = importModal.element.find('input[name="csv_filename"]');

    // Selecting a file
    selectFileInput.change(function (e) {
        var file = $(this).prop('files')[0];
        if (file) {
            selectedFileInput.val(file.name);
        }
    });

    // Clicking the import button
    importButton.on('click', function () {
        importModal.errorMessage(false);
        importModal.formData({ 'csv_filename' : '', 'csv_file': '', })
        importModal.open(true);
    });
    
    // Clicking the export button
    exportButton.on('click', function () {
        exportButton.prop('disabled', true).css('pointer-events', 'none').text('Exporting Tariff...');
        touringTariffDAO.export(function (response) {
            if (response.success) {
                var result = Papa.unparse(response.data, {
                    header: true,
                    download: true,
                });
                var csvData = new Blob([result], {type: 'text/csv;charset=utf-8;'});
                var csvURL = window.URL.createObjectURL(csvData);
                var testLink = document.createElement('a');
                testLink.download = "touring-tariff-export.csv";
                testLink.href = csvURL;
                testLink.click();
            } else {
                $.notification('Error exporting tariff.');
            }
            exportButton.prop('disabled', false).css('pointer-events', 'initial').text('Export Tariff');
        });
    });

    // Generate example CSV
    exampleDownloadBtn.on('click', function (e) {
        e.preventDefault();
        exampleDownloadBtn.prop('disabled', true).css('pointer-events', 'none').text('Downloading Template...');
        touringTariffDAO.getExample(function (response) {
            var result = Papa.unparse(response, {
                header: true,
                download: true,
            });
            var csvData = new Blob([result], {type: 'text/csv;charset=utf-8;'});
            var csvURL = window.URL.createObjectURL(csvData);
            var testLink = document.createElement('a');
            testLink.download = "touring-tariff-import.csv";
            testLink.href = csvURL;
            testLink.click();
            exampleDownloadBtn.prop('disabled', false).css('pointer-events', 'initial').text('Downloading Template');
        });
    });

    // Clicking the clear button
    clearButton.on('click', function () {
        clearModal.open(true);
        clearModal.loading(false);
        clearModal.disabled(false);
    });

    // Get the current tariff info
    function renderCurrentTariffInfo () {
        touringTariffDAO.getCount(function (response) {
            currentTariffInfo.text('Current tariff: ' + response + ' rows');
        });
    }

    // Run some functions...
    renderCurrentTariffInfo();

})(jQuery);
