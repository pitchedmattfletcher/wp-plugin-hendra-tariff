(function ($) {

    // Init DAOs
    var rangesDAO = new HendraTariff.DAOs.RangesDAO();

    // Reference some elements
    var createRangeBtn = $('.hendra-tariff-action-btn.create.range');
    var rangesTable = $('.hendra-tariff-ranges-table');

    // Init tablefy on ranges
    rangesTable.tablefy({
        'columnModel': [
            { label: "ID", field_name: "id", width: '120px' },
            { label: "Name", field_name: "name" },
            { label: "Order", field_name: "order" },
            { label: "Update", field_name: "update", align: "center", width: "120px", action: openUpdateRangeModal },
            { label: "Delete", field_name: "delete", align: "center", width: "120px", action: openDeleteRangeModal }
        ]
    });

    // Init the create range modal.
    var createRangeModal = $.formModal({

        // The modal title
        'title': 'Create Range',

        // Set the submit button text
        'submitButtonLabel': 'Create',

        // The form template
        'formTemplate':
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Range Name</div>' +
                '<input type="text" name="name" value="" placeholder="E.g. Premium"  />' +
            '</div>' + 
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Order (Lowest orders are displayed first)</div>' +
                '<input type="number" name="order" value="0" placeholder="1"  />' +
            '</div>',

        // Handle the on submit event
        'onSubmit': function (instance, data) {

            // Disable form, and show loading icon
            instance.disabled(true);
            instance.loading(true);

            // Create the new range
            rangesDAO.createRange({ name: data.name, order: data.order, }, function (response) {
 
                // Form is no longer loading and disabled
                instance.disabled(false);
                instance.loading(false);
                
                // If there was an error
                if (!response.success) {
                    instance.errorMessage(response.message);
                } else {
                    instance.errorMessage(false);
                    instance.open(false);
                    $.notification(response.message);
                    renderRanges();
                }

            });
        }
    });

    // Init the edit range modal
    var editRangeModal = $.formModal({

        // The modal title
        'title': 'Edit Range',

        // Set the submit button text
        'submitButtonLabel': 'Save',

        // The form template
        'formTemplate':
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Range ID</div>' +
                '<input type="text" name="id" value="" readonly/>' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Range Name</div>' +
                '<input type="text" name="name" value="" placeholder="E.g. Premium"  />' +
            '</div>' + 
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Order (Lowest orders are displayed first)</div>' +
                '<input type="number" name="order" value="0" placeholder="1"  />' +
            '</div>',

        // Handle the on submit event
        'onSubmit': function (instance, data) {

            // Disable form, and show loading icon
            instance.disabled(true);
            instance.loading(true);

            // Create the new range
            rangesDAO.updateRange({ name: data.name, id: data.id, order: data.order }, function (response) {
    
                // Form is no longer loading and disabled
                instance.disabled(false);
                instance.loading(false);

                // If there was an error
                if (!response.success) {
                    instance.errorMessage(response.message);
                } else {
                    instance.errorMessage(false);
                    instance.open(false);
                    $.notification(response.message);
                    renderRanges();
                }

            });
        }
    });

    // Init the delete range modal
    var deleteRangeModal = $.formModal({

        // The modal title
        'title': 'Delete Range',

        // Set the submit button text
        'submitButtonLabel': 'Delete',

        // The form template
        'formTemplate':
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Range ID</div>' +
                '<input type="text" name="id" value="" readonly />' +
            '</div>' +
            '<div class="form-modal-group">' +
                '<div class="form-modal-label">Range Name</div>' +
                '<input type="text" name="name" value="" readonly />' +
            '</div>' + 
            '<div class="form-modal-desc">Deleting this range will also delete all Caravans assigned to it.</div>',

        // Handle the on submit event
        'onSubmit': function (instance, data) {

            // Disable form, and show loading icon
            instance.disabled(true);
            instance.loading(true);

            // Create the new range
            rangesDAO.deleteRange(data.id, function (response) {
    
                // Form is no longer loading and disabled
                instance.disabled(false);
                instance.loading(false);

                // If there was an error
                if (!response.success) {
                    instance.errorMessage(response.message);
                } else {
                    instance.errorMessage(false);
                    instance.open(false);
                    $.notification(response.message);
                    renderRanges();
                }

            });
        }
    });

    // Add click event to the create range button
    createRangeBtn.on('click', openCreateRangeModal);


    function openCreateRangeModal () {
        createRangeModal.formData({ 'name': '' });
        createRangeModal.open(true);
    }

    function openUpdateRangeModal (range) {
        if (range.id === '-')
            return;
        editRangeModal.formData({
            id: range.id,
            name: range.name,
            order: range.order,
        });
        editRangeModal.open(true);
    }

    function openDeleteRangeModal (range) {
        if (range.id === '-')
            return;
        deleteRangeModal.formData({
            id: range.id,
            name: range.name,
        });
        deleteRangeModal.open(true);
    }

    function renderRanges () {
        rangesTable.tablefy('rowModel', [ { id: '-', name: '-', update: '-', order: '-', 'delete': '-' } ]);
        rangesDAO.getRanges(function (response) {
            var rowModel = [];
            for (var i = 0; i < response.data.length; i++) {
                rowModel.push({
                    id: response.data[i].id,
                    name: response.data[i].name,
                    order: response.data[i].order,
                    update: '<span class="tablefy-icon update"></span>',
                    delete: '<span class="tablefy-icon delete"></span>'
                })
            }
            rowModel = rowModel.length > 0 ? rowModel : [{ id: '-', name: '-', order: '-', update: '-', 'delete': '-' }]
            rangesTable.tablefy('rowModel', rowModel);
        });
    }

    renderRanges();

})(jQuery);
