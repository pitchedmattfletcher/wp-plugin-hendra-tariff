if (typeof window.HendraTariff === 'undefined') {
    window.HendraTariff = {};
}
if (typeof window.HendraTariff.DAOs === 'undefined') {
    window.HendraTariff.DAOs = {};
}


window.HendraTariff.DAOs.PersonTypesDAO = function () {

    this.getPersonTypes = function (callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_get_person_types',
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

    this.createPersonType = function (personType, callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_create_person_type',
                args: {
                    person_type: personType
                }
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

    this.updatePersonType = function (personType, callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_update_person_type',
                args: {
                    person_type: personType
                }
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

    this.deletePersonType = function (personTypeID, callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_delete_person_type',
                args: {
                    person_type_id: personTypeID
                }
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

}