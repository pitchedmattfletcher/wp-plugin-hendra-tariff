if (typeof window.HendraTariff === 'undefined') {
    window.HendraTariff = {};
}
if (typeof window.HendraTariff.DAOs === 'undefined') {
    window.HendraTariff.DAOs = {};
}


window.HendraTariff.DAOs.RangesDAO = function () {

    this.getRanges = function (callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_get_ranges',
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

    this.createRange = function (range, callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_create_range',
                args: {
                    range: range
                }
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

    this.updateRange = function (range, callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_update_range',
                args: {
                    range: range
                }
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

    this.deleteRange = function (rangeID, callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_delete_range',
                args: {
                    range_id: rangeID
                }
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

}