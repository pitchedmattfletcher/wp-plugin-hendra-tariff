if (typeof window.HendraTariff === 'undefined') {
    window.HendraTariff = {};
}
if (typeof window.HendraTariff.DAOs === 'undefined') {
    window.HendraTariff.DAOs = {};
}

window.HendraTariff.DAOs.GlampingTariffDAO = function () {

    this.import = function (rows, callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_glamping_import',
                args: {
                    rows: rows,
                }
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

    this.export = function (callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_glamping_export',
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

    this.clear = function (callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_glamping_clear',
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

    this.getExample = function (callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_glamping_get_example',
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

    this.getCount = function (callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_glamping_get_count',
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }


}