if (typeof window.HendraTariff === 'undefined') {
    window.HendraTariff = {};
}
if (typeof window.HendraTariff.DAOs === 'undefined') {
    window.HendraTariff.DAOs = {};
}


window.HendraTariff.DAOs.DatesConfigDAO = function () {

    this.getDatesConfig = function (callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_get_dates_config',
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

    this.createDateConfig = function (dateConfig, callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_create_date_config',
                args: {
                    date_config: dateConfig
                }
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

    this.deleteDateConfig = function (dateConfigID, callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_delete_date_config',
                args: {
                    date_config_id: dateConfigID
                }
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

}