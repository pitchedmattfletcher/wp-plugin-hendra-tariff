if (typeof window.HendraTariff === 'undefined') {
    window.HendraTariff = {};
}
if (typeof window.HendraTariff.DAOs === 'undefined') {
    window.HendraTariff.DAOs = {};
}


window.HendraTariff.DAOs.ExtrasDAO = function () {

    this.getExtras = function (callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_get_extras',
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

    this.createExtra = function (extra, callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_create_extra',
                args: {
                    extra: extra
                }
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

    this.updateExtra = function (extra, callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_update_extra',
                args: {
                    extra: extra
                }
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

    this.deleteExtra = function (extraID, callback) {
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: {
                action: 'hendra_tariff_delete_extra',
                args: {
                    extra_id: extraID
                }
            },
            complete: function (event, xhr) {
                if (typeof callback === 'function') {
                    callback.call(this, event.responseJSON);
                }
            }
        });
    }

}