(function ($) {
	
	// Append nth to a number
	function getNumberWithOrdinal(n) {
		var s = ["th", "st", "nd", "rd"],
			v = n % 100;
	  	return n + (s[(v - 20) % 10] || s[v] || s[0]);
	}
	
	var DAY_NAMES = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
	var MONTH_NAMES = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
    var MONTH_NAMES_LONG = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ]

    // Set the plugin name
    var PLUGIN_NAME = "hendraTariff";

    // HTML Template
    var TEMPLATE = 
        '<div class="hendra-tariff-filter-section">' +
            '<div class="hendra-tariff-filter-select-container">' +
                '<div class="hendra-tariff-filter-select-label">Holiday Type</div>' +
                '<select class="hendra-tariff-filter-select hendra-tariff-filter-select-holiday-type no-jcf">' + 
                    '<option value="holiday_homes">Holiday Homes</option>' + 
                    '<option value="touring">Touring and Camping</option>' + 
                    '<option value="glamping">Glamping Pods</option>' + 
                '</select>' +
            '</div>' + 
            '<div class="hendra-tariff-filter-select-container">' +
                '<div class="hendra-tariff-filter-select-label">Month</div>' +
                '<select class="hendra-tariff-filter-select hendra-tariff-filter-select-year-month no-jcf">' + 
                '</select>' +
            '</div>' +
        '</div>' + 
        '<div class="hendra-tariff-key-section">' + 
            '<div class="hendra-tariff-key-item">' + 
                '<i class="fal fa-calendar-times text-danger"></i> = Sorry, fully booked please call 01637 875778' + 
            '</div>' + 
        '</div>' + 
        '<div class="hendra-tariff-table-section"></div>';
    
    // Set the plugin defaults
    var PLUGIN_DEFAULTS = {
        selectedHolidayType: 'holiday_homes',
    };

    // Private defaults
    var PRIVATES = {
        ajaxRequest: null,
    }
    
    // Plugin constructor
    function Plugin (element, options) {
        this.settings = $.extend({}, PLUGIN_DEFAULTS, options);
        this.privates = $.extend({}, PRIVATES, {});
        this.element = $(element);
        this.element.addClass('hendra-tariff-container');
        this.element.append(TEMPLATE);
        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {

        init: function() {
            var that = this;

            // Make request to get holiday homes tariff for the selected month
            this.privates.ajaxRequest = $.ajax({
                type: 'POST',
                url: "/wp-admin/admin-ajax.php",
                dataType: 'json',
                data: {
                    action: 'hendra_tariff_get_dates_config',
                },
                complete: function (event, xhr) {

                    // If xhr was aborted, don't run the code below.
                    if (xhr === 'abort') {
                        return;
                    }

                    // Get response
                    var response = event.responseJSON;
                    if (response.data && response.data.length > 0) {

                        // Build select box
                        that.element.find('.hendra-tariff-filter-select-year-month').html('');
                        for (var i = 0; i < response.data.length; i++) {
                            that.element.find('.hendra-tariff-filter-select-year-month').append('<option data-year="' + response.data[i].year + '" data-month="' + response.data[i].month + '" value="' + response.data[i].month + '">' + response.data[i].year + ' ' + MONTH_NAMES_LONG[response.data[i].month -1 ] + '</option>');
                        }

                        // Holiday type or date on cahnge
                        that.element.find('.hendra-tariff-filter-select-holiday-type, .hendra-tariff-filter-select-year-month').on('change', function () {
                            var selectedType = that.element.find('.hendra-tariff-filter-select-holiday-type').find("option:selected");
                            var selectedDate = that.element.find('.hendra-tariff-filter-select-year-month').find("option:selected");
                            that.getTariff(selectedType.val(), selectedDate.data('year'), selectedDate.data('month'));
                        }).change();

                    } else {
                        // No dates configured
                    }
                        
                    // Set ajax request back to null
                    that.privates.ajaxRequest = null;

                }
            });

        },

        getTariff: function (type, year, month) {
            var that = this;

            // Hide key
            this.element.find('.hendra-tariff-key-section').hide();

            // Reference table section
            var tableSection = this.element.find('.hendra-tariff-table-section');

            // Empty table section and show a loader
            tableSection.empty();
            tableSection.append('<div class="hendra-tariff-table-loader"></div>');

            // If ajax request is already running, abort it.
            if (this.privates.ajaxRequest !== null) 
                this.privates.ajaxRequest.abort();

            // If selected holiday type is holiday homes
            if (type === 'holiday_homes') {

                // Make request to get holiday homes tariff for the selected month
                this.privates.ajaxRequest = $.ajax({
                    type: 'POST',
                    url: "/wp-admin/admin-ajax.php",
                    dataType: 'json',
                    data: {
                        action: 'hendra_tariff_holiday_homes_get_by_year_month',
                        args: {
                            year: year,
                            month: month
                        }
                    },
                    complete: function (event, xhr) {

                        // If xhr was aborted, don't run the code below.
                        if (xhr === 'abort') {
                            return;
                        }

                        // Get response
                        var response = event.responseJSON;
                        if (response.data.length == 0) {
                            tableSection.html('<h3 class="hendra-tariff-no-results">No results found. Please try a different month.</h3>');
                        } else {
                            that.element.find('.hendra-tariff-key-section').show();
                            var data = that.processHolidayHomesTariff(response.data);
                            that.renderHolidayHomesTariff(data);
                        }

                        // Set ajax request back to null
                        that.privates.ajaxRequest = null;
                    }
                });
            }

            // If selected holday type is touring
            else if (type === 'touring') {

                // Make request to get touring tariff for the selected month
                this.privates.ajaxRequest = $.ajax({
                    type: 'POST',
                    url: "/wp-admin/admin-ajax.php",
                    dataType: 'json',
                    data: {
                        action: 'hendra_tariff_touring_get_by_year_month',
                        args: {
                            year: year,
                            month: month
                        }
                    },
                    complete: function (event, xhr) {

                        // If xhr was aborted, don't run the code below.
                        if (xhr === 'abort') {
                            return;
                        }

                        // Get response
                        var response = event.responseJSON;
                        if (response.data.length == 0) {
                            tableSection.html('<h3 class="hendra-tariff-no-results">No results found. Please try a different month.</h3>');
                        } else {
                            var data = that.processTouringTariff(response.data);
                            that.renderTouringTariff(data);
                        }

                        // Set ajax request back to null
                        that.privates.ajaxRequest = null;
                    }
                });
            }

            // If selected holday type is glamping
            else if (type === 'glamping') {

                // Make request to get touring tariff for the selected month
                this.privates.ajaxRequest = $.ajax({
                    type: 'POST',
                    url: "/wp-admin/admin-ajax.php",
                    dataType: 'json',
                    data: {
                        action: 'hendra_tariff_glamping_get_by_year_month',
                        args: {
                            year: year,
                            month: month
                        }
                    },
                    complete: function (event, xhr) {

                        // If xhr was aborted, don't run the code below.
                        if (xhr === 'abort') {
                            return;
                        }

                        // Get response
                        var response = event.responseJSON;
                        if (response.data.length == 0) {
                            tableSection.html('<h3 class="hendra-tariff-no-results">No results found. Please try a different month.</h3>');
                        } else {
                            var data = that.processGlampingTariff(response.data);
                            that.renderGlampingTariff(data);
                        }

                        // Set ajax request back to null
                        that.privates.ajaxRequest = null;
                    }
                });
            }

        },

        processHolidayHomesTariff: function (data) {

            var uniqueDurations = [];
            var uniqueRanges = [];
            for (var i = 0; i < data.length; i++) {

                // Find unique durations
                var duration = parseInt(data[i].duration);
                if (!this._arrayContains(uniqueDurations, duration, 'duration')) {
                    uniqueDurations.push({
                        duration: duration,
                        dates: [],
                    });
                }

                // Find unique dates per duration
                var arrival_date = data[i].arrival_date;
                for (var j = 0; j < uniqueDurations.length; j++) {
                    if (uniqueDurations[j].duration == parseInt(data[i].duration)) {
                        if (!this._arrayContains(uniqueDurations[j].dates, arrival_date)) {
                            uniqueDurations[j].dates.push(arrival_date);
                        }
                    }
                }

                // Find unique ranges
                var range = data[i].range_name;
                if (!this._arrayContains(uniqueRanges, range, 'name')) {
                    uniqueRanges.push({
                        name: range,
                        statics: [],
                    });
                }

                // Find unique statics per range
                var static_name = data[i].static_name;
                for (var j = 0; j < uniqueRanges.length; j++) {
                    if (uniqueRanges[j].name == data[i].range_name) {
                        if (!this._arrayContains(uniqueRanges[j].statics, static_name, 'name')) {
                            uniqueRanges[j].statics.push({
                                name: static_name,
                                flash: data[i].static_flash,
                                models: [],
                            });
                        }
                    }
                }

                // Find unique models per static
                for (var j = 0; j < uniqueRanges.length; j++) {
                    if (uniqueRanges[j].name == data[i].range_name) {
                        for (var k = 0; k < uniqueRanges[j].statics.length; k++) {
                            if (uniqueRanges[j].statics[k].name == data[i].static_name) {
                                if (!this._arrayContains(uniqueRanges[j].statics[k].models, data[i].model_name)) {
                                    uniqueRanges[j].statics[k].models.push(data[i].model_name);
                                }
                            }
                        }

                    }
                }
                
            }

            return {
                durations: uniqueDurations,
                ranges: uniqueRanges,
                calendar: data,
            }

        },

        processTouringTariff: function (data) {

            // Find unique dates
            var uniqueDates = [];
            for (var i = 0; i < data.length; i++) {

                // Find unique durations
                var date = data[i].arrival_date;
                if (!this._arrayContains(uniqueDates, date, 'date')) {
                    uniqueDates.push({
                        date: date,
                        charges: [],
                    });
                }

                // Now iterate through the found unique dates
                // and if this row exists in the unique dates array,
                // append the data.
                for (var j = 0; j < uniqueDates.length; j++) {
                    if (uniqueDates[j].date === data[i].arrival_date) {
                        uniqueDates[j].charges.push({
                            name: data[i].name,
                            code: data[i].code,
                            type: data[i].type,
                            price: data[i].price,
                            discount: data[i].discount
                        });
                        break;
                    }
                }

            }

            // Return result
            return uniqueDates;
        },

        processGlampingTariff: function (data) {

            // Find unique dates
            var uniqueDates = [];
            for (var i = 0; i < data.length; i++) {

                // Find unique durations
                var date = data[i].arrival_date;
                if (!this._arrayContains(uniqueDates, date, 'date')) {
                    uniqueDates.push({
                        date: date,
                        charges: [],
                    });
                }

                // Now iterate through the found unique dates
                // and if this row exists in the unique dates array,
                // append the data.
                for (var j = 0; j < uniqueDates.length; j++) {
                    if (uniqueDates[j].date === data[i].arrival_date) {
                        uniqueDates[j].charges.push({
                            name: data[i].name,
                            code: data[i].code,
                            price: data[i].price,
                            discount: data[i].discount
                        });
                        break;
                    }
                }

            }

            console.log(uniqueDates)

            // Return result
            return uniqueDates;
        },

        renderHolidayHomesTariff: function (data) {


            // Reference table section
            var tableSection = this.element.find('.hendra-tariff-table-section');

            // Empty table section
            tableSection.empty();

            // Iterate through all durations
            for (var i = 0; i < data.durations.length; i++) {

                // Create a table for this duration
                // with a header.
                var table = $('<table class="hendra-tariff-holiday-homes-table" data-duration="' + data.durations[i].duration + '">' +
                    '<tbody></tbody>' +
                '</table>');

                // Create the headers
                var rangesTR = $('<tr><td class="hendra-tariff-table-range sticky-col" colspan="3"></td></tr>');
                var staticsTR = $('<tr><td class="hendra-tariff-table-static sticky-col" colspan="3"></td></tr>');
                var modelsTR = $('<tr><td class="hendra-tariff-table-model sticky-col" colspan="3"></td></tr>');
                for (var j = 0; j < data.ranges.length; j++) {

                    // Ranges header  
                    var rangeAmountModels = 0;
                    for (var k = 0; k < data.ranges[j].statics.length; k++) {
                        rangeAmountModels += data.ranges[j].statics[k].models.length;
                    }
                    rangesTR.append('<td class="hendra-tariff-table-range" colspan="' + rangeAmountModels + '">' + data.ranges[j].name + '</td>');
                
                    // Staics header
                    for (var k = 0; k < data.ranges[j].statics.length; k++) {
                        var amountStaticModels = data.ranges[j].statics[k].models.length;
                        var flash = data.ranges[j].statics[k].flash;
                        if (flash === null) {
                            staticsTR.append('<td class="hendra-tariff-table-static" colspan="' + amountStaticModels + '">' + data.ranges[j].statics[k].name + '</td>');
                        } else {
                            staticsTR.append('<td class="hendra-tariff-table-static" colspan="' + amountStaticModels + '">' + data.ranges[j].statics[k].name + '<br><span class="flash">' + flash + '<span></td>');
                        }
                    }

                    // Models header
                    for (var k = 0; k < data.ranges[j].statics.length; k++) {
                        for (var l = 0; l < data.ranges[j].statics[k].models.length; l++) {
                            modelsTR.append('<td class="hendra-tariff-table-model" colspan="1">' + data.ranges[j].statics[k].models[l] + '</td>');
                        }
                    }
                    
                }
                table.find('tbody').append(rangesTR);
                table.find('tbody').append(staticsTR);
                table.find('tbody').append(modelsTR);

                // For each table of duration, append a row with the date,
                // and a column for each model.
                for (var j = 0; j < data.durations[i].dates.length; j++) {
					
					// Convert DD/MM/YYYY to user friendly string
					var dateSplit = data.durations[i].dates[j].split('/');
					var dateObject = new Date(parseInt(dateSplit[2]), parseInt(dateSplit[1]) - 1, parseInt(dateSplit[0]));
					var dayName = DAY_NAMES[dateObject.getDay()];
					var day = getNumberWithOrdinal(dateSplit[0]);
					var month = MONTH_NAMES[dateObject.getMonth()];
					var year = dateSplit[2];
					var dateStr = dayName + ' ' + day + ' ' + month + ', ' + year;
					
                    var dateTR = $('<tr tabindex="1"></tr>');
                    dateTR.append('<td class="hendra-tariff-table-date sticky-col" colspan="3">' + dateStr + '</td>');
                    for (var m = 0; m < data.ranges.length; m++) {
                        for (var k = 0; k < data.ranges[m].statics.length; k++) {
                            for (var l = 0; l < data.ranges[m].statics[k].models.length; l++) {
                                dateTR.append('<td class="hendra-tariff-table-price" colspan="1" data-duration="' + data.durations[i].duration + '" data-date="' + data.durations[i].dates[j] + '" data-model="' + data.ranges[m].statics[k].models[l] + '">-</td>');
                            }
                        }
                    }
                    table.find('tbody').append(dateTR);
                }

                // Set arrival day labels
                var arrive_day = '(Sat - Sat)';
                
                if (data.durations[i].duration == 4) {
                    var arrive_day = '(Mon - Fri)';
                }
                if (data.durations[i].duration == 3) {
                    var arrive_day = '(Fri - Mon)';
                }

                // Append the duration table
                var tableContainer = $('<div class="hendra-tariff-table-container"><div class="hendra-tariff-table-heading">' + data.durations[i].duration + ' Night ' + arrive_day + ' Holiday Homes Tariff</div></div>');
                tableContainer.append(table);
                tableSection.append(tableContainer);

                // Attach on scroll listener
                table.on('scroll', function (e) {
                    if ($(this).scrollLeft() > 0) {
                        $(this).addClass('sticky-on');
                    } else {
                        $(this).removeClass('sticky-on');
                    }
                });

            }

            // Tables are now built.
            // We now just need to look through the main data
            // and place the correct rows in the correct table cells.
            for (var i = 0; i < data.calendar.length; i++) {
                var cell = tableSection[0].querySelector('td[data-date="' + data.calendar[i].arrival_date + '"][data-duration="' + data.calendar[i].duration + '"][data-model="' + data.calendar[i].model_name + '"] ');
                if (cell !== null) {
                    var fullyBooked = data.calendar[i].fully_booked === '1' ? true : false;
                    if (fullyBooked) {
                        cell.innerHTML = '<span title="Fully booked"><i class="fal fa-calendar-times text-danger"></i></span>';
                    } else {
                        var price = parseFloat(data.calendar[i].price);
                        var discount = parseFloat(data.calendar[i].discount);
                        if (discount > 0) {
                            cell.innerHTML = '<span class="was">£' + (price.toFixed(0)) + '</span><br>£<span>' + (price - discount).toFixed(0) + '</span>';
                        } else {
                            cell.innerHTML = '<span>£' + price.toFixed(0) + '</span>';
                        }
                    }
                    
                }
            }

            
        },

        renderTouringTariff: function (dates) {

            // hide key
            this.element.find('.hendra-tariff-key-section').hide();

            // Reference table section
            var tableSection = this.element.find('.hendra-tariff-table-section');

            // Empty table section
            tableSection.empty();

            // Create the table
            var table = $("<table class='hendra-tariff-touring-table'><tbody></tbody></table>");

            // Iterate through all arrival dates
            var uniqueChargeTypes = [];
            var chargesHeadings = $('<tr><td class="hendra-tariff-table-charge-type sticky-col"></td></tr>');
            var chargesRows = '';
            for (var i = 0; i < dates.length; i++) {
				
				// Convert DD/MM/YYYY to user freindly string
				var dateSplit = dates[i].date.split('/');
				var dateObject = new Date(parseInt(dateSplit[2]), parseInt(dateSplit[1]) - 1, parseInt(dateSplit[0]));
				var dayName = DAY_NAMES[dateObject.getDay()];
				var day = getNumberWithOrdinal(dateSplit[0]);
				var year = dateSplit[2];
				var month = MONTH_NAMES[dateObject.getMonth()];
				var dateStr = dayName + ' ' + day + ' ' + month + ', ' + year;

                // Create the row of charges
                var chargesRow = '<tr tabindex="1" ><td class="hendra-tariff-table-date sticky-col">' + dateStr + '</td>';

                // Iterate through charge types and find unique charges.
                // If it's unique create the table heading
                for (var j = 0; j < dates[i].charges.length; j++) {
                    if (!this._arrayContains(uniqueChargeTypes, dates[i].charges[j].code)) {
                        uniqueChargeTypes.push(dates[i].charges[j].code);
                        chargesHeadings.append('<td class="hendra-tariff-table-charge-type ' + dates[i].charges[j].type + '">' + dates[i].charges[j].name + '</td>')
                    }

                    // Loop through charges again and create table row
                    var price = parseFloat(dates[i].charges[j].price);
                    var discount = parseFloat(dates[i].charges[j].discount);
                    var type = dates[i].charges[j].type;
                    if (discount > 0) {
                        chargesRow += '<td class="hendra-tariff-table-price"><span class="was">£' + (price.toFixed(2)) + '</span><br>£<span>' + (price - discount).toFixed(2) + '</span></td>';
                    } else {
                        if (type === 'person_type' || type === 'extra') {
                            if (price > 0) {
                                chargesRow += '<td class="hendra-tariff-table-price"><span>£' + (price.toFixed(2)) + '</span></td>';
                            } else {
                                chargesRow += '<td class="hendra-tariff-table-price"><span>FREE</span></td>';
                            }
                        } else if (type === 'pitch_type') {
                            if (price > 0) {
                                chargesRow += '<td class="hendra-tariff-table-price"><span>£' + (price.toFixed(2)) + '</span></td>';
                            } else {
                                chargesRow += '<td class="hendra-tariff-table-price"><span>N/A</span></td>';
                            }
                        }
                    }
                }

                // Append new row
                chargesRows += chargesRow + '</tr>';

            }

            // Create table out generated HTML
            var tableContainer = $('<div class="hendra-tariff-table-container"><div class="hendra-tariff-table-heading">Nightly Touring Tariff</div></div>');
            table.find('tbody').append(chargesHeadings);
            table.find('tbody').append(chargesRows);
            tableContainer.append(table);
            tableSection.append(tableContainer);

            // Add the on scroll event
            table.on('scroll', function (e) {
                if ($(this).scrollLeft() > 0) {
                    $(this).addClass('sticky-on');
                } else {
                    $(this).removeClass('sticky-on');
                }
            });

        },

        renderGlampingTariff: function (dates) {


            // Reference table section
            var tableSection = this.element.find('.hendra-tariff-table-section');

            // Empty table section
            tableSection.empty();

            // Create the table
            var table = $("<table class='hendra-tariff-glamping-table'><tbody></tbody></table>");

            // Iterate through all arrival dates
            var uniqueChargeTypes = [];
            var chargesHeadings = $('<tr><td class="hendra-tariff-table-charge-type sticky-col"></td></tr>');
            var chargesRows = '';
            for (var i = 0; i < dates.length; i++) {

				
				// Convert DD/MM/YYYY to user freindly string
				var dateSplit = dates[i].date.split('/');
				var dateObject = new Date(parseInt(dateSplit[2]), parseInt(dateSplit[1]) - 1, parseInt(dateSplit[0]));
				var dayName = DAY_NAMES[dateObject.getDay()];
				var day = getNumberWithOrdinal(dateSplit[0]);
				var year = dateSplit[2];
				var month = MONTH_NAMES[dateObject.getMonth()];
				var dateStr = dayName + ' ' + day + ' ' + month + ', ' + year;
				
                // Create the row of charges
                var chargesRow = '<tr tabindex="1" ><td class="hendra-tariff-table-date sticky-col">' + dateStr + '</td>';

                // Iterate through charge types and find unique charges.
                // If it's unique create the table heading
                for (var j = 0; j < dates[i].charges.length; j++) {
                    if (!this._arrayContains(uniqueChargeTypes, dates[i].charges[j].code)) {
                        uniqueChargeTypes.push(dates[i].charges[j].code);
                        chargesHeadings.append('<td class="hendra-tariff-table-charge-type ' + dates[i].charges[j].type + '">' + dates[i].charges[j].name + '</td>')
                    }

                    // Loop through charges again and create table row
                    var price = parseFloat(dates[i].charges[j].price);
                    var discount = parseFloat(dates[i].charges[j].discount);
                    if (discount > 0) {
                        chargesRow += '<td class="hendra-tariff-table-price"><span class="was">£' + (price.toFixed(2)) + '</span><br>£<span>' + (price - discount).toFixed(2) + '</span></td>';
                    } else {
                        if (price > 0) {
                            chargesRow += '<td class="hendra-tariff-table-price"><span>£' + (price.toFixed(2)) + '</span></td>';
                        } else {
                            chargesRow += '<td class="hendra-tariff-table-price"><span>FREE</span></td>';
                        }
                    }
                }

                // Append new row
                chargesRows += chargesRow + '</tr>';

            }

            // Create table out generated HTML
            var tableContainer = $('<div class="hendra-tariff-table-container"><div class="hendra-tariff-table-heading">Nightly Pods Tariff</div></div>');
            table.find('tbody').append(chargesHeadings);
            table.find('tbody').append(chargesRows);
            tableContainer.append(table);
            tableSection.append(tableContainer);

            // Add the on scroll event
            table.on('scroll', function (e) {
                if ($(this).scrollLeft() > 0) {
                    $(this).addClass('sticky-on');
                } else {
                    $(this).removeClass('sticky-on');
                }
            });
        },

        _arrayContains: function (array, value, key) {
            if (typeof key === 'undefined') {
                for (var i = 0; i < array.length; i++) {
                    if (array[i] == value) {
                        return true;
                    }
                }
                return false;
            } else {
                for (var i = 0; i < array.length; i++) {
                    if (array[i][key] == value) {
                        return true;
                    }
                }
                return false;
            }

        },


    });

    // You don't need to change anything below:
    $.fn[PLUGIN_NAME] = function ( options ) {
        var args = arguments;
        if (options === undefined || typeof options === 'object') {
            return this.each(function () {
                if (!$.data(this, 'plugin_' + PLUGIN_NAME)) {
                    $.data(this, 'plugin_' + PLUGIN_NAME, new Plugin( this, options ));
                }
            });
        } else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {
            var returns;
            this.each(function () {
                var instance = $.data(this, 'plugin_' + PLUGIN_NAME);
                if (instance instanceof Plugin && typeof instance[options] === 'function') {
                    returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1 ) );
                }
                if (options === 'destroy') {
                    $.data(this, 'plugin_' + PLUGIN_NAME, null);
                }
            });
            return returns !== undefined ? returns : this;
        }
    };

    // Auto init plugin because its a wordpress plugin
    $('.hendra-tariff-shortcoded').hendraTariff();

})(jQuery);